# F500 Data Analytics

## Project Description

The F500 Data Analytics project provides a suite of tools and scripts for processing, analyzing, and visualizing point cloud data, particularly from Phenospex PLY PointCount files. The project leverages libraries such as Open3D and NumPy to handle 3D data and perform operations like NDVI computation and visualization. Additionally, it includes functionalities for interacting with the Fairdom SEEK API to manage data resources.

## Table of Contents

- [Installation Instructions](#installation-instructions)
- [Usage Guide](#usage-guide)
- [Features](#features)
- [Modules Overview](#modules-overview)
- [Configuration & Customization](#configuration--customization)
- [Testing & Debugging](#testing--debugging)
- [Contributing Guide](#contributing-guide)
- [License & Author Information](#license--author-information)

## Installation Instructions

To set up the project, ensure you have Python installed on your system. Then, install the required dependencies using pip:

```bash
pip install open3d numpy requests pandas
```

## Usage Guide

### Visualizing Point Cloud Data

To visualize point cloud data and compute NDVI, use the `visualization_ply.py` script:

```bash
python analytics/visualizations/visualization_ply.py <path_to_ply_file>
```

### Deleting Resources via Fairdom SEEK API

To delete resources from a Fairdom SEEK server, use the `deleteFAIRObject.py` script:

```bash
python analytics/f500/collecting/deleteFAIRObject.py <token>
```

### F500 Toolkit

The `toolkit.py` script provides a command-line interface for various data processing tasks:

```bash
python analytics/f500/collecting/toolkit.py <command>
```

Available commands include:
- `restructure`
- `pointclouds`
- `verify`
- `histogram`
- `upload`

## Features

- **Point Cloud Visualization**: Visualize and process 3D point cloud data.
- **NDVI Computation**: Compute and visualize NDVI from point cloud data.
- **Resource Management**: Interact with Fairdom SEEK API to manage data resources.
- **Data Processing**: Restructure, verify, and upload data using the F500 toolkit.

## Modules Overview

- **visualization_ply.py**: Handles point cloud visualization and NDVI computation.
- **clearWhites.py**: Loads point cloud data for further processing.
- **deleteFAIRObject.py**: Deletes resources from Fairdom SEEK server.
- **toolkit.py**: Command-line interface for F500 data processing tasks.

## Configuration & Customization

- **API Token**: Ensure you have a valid authorization token for accessing the Fairdom SEEK API.
- **File Paths**: Provide correct paths to PLY files when using visualization scripts.

## Testing & Debugging

- **Error Handling**: Scripts include basic error handling for file paths and API requests.
- **Future Work**: Consider adding more robust error handling and automated tests for critical functionalities.

## Contributing Guide

Contributions are welcome! Please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Commit your changes with clear messages.
4. Push your changes to your fork.
5. Submit a pull request with a detailed description of your changes.

## License & Author Information

This project is licensed under the MIT License. For more information, see the LICENSE file.

Author: Sven Warris

---

This README provides a comprehensive overview of the F500 Data Analytics project, including installation, usage, and contribution guidelines. Feel free to update sections as the project evolves.