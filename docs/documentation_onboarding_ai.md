# Onboarding Guide for New Developers

Welcome to the F500 Data Analytics project! This guide will help you get started with understanding the codebase, focusing on key modules, and following the typical development workflow. The F500 project is designed to process, analyze, and visualize point cloud data, particularly from Phenospex PLY PointCount files.

## Codebase Structure

The codebase is organized into several modules, each serving a specific purpose in the data processing pipeline. Below is an overview of the key modules:

1. **rescaleWavelength.py**: Contains functions to rescale wavelengths in point cloud data.
2. **computePhenotypes.py**: Provides functions to compute various indices like NDVI, NPCI, and greenness for phenotypic analysis.
3. **histograms_ply.py**: Handles the creation of PNG images from point clouds.
4. **animate_ply.py**: Manages animations of point clouds using Open3D visualization tools.
5. **visualization_ply.py**: Main function for executing point cloud processing and visualization.
6. **clearWhites.py**: Reads PLY files and loads them into Open3D PointCloud objects.
7. **PointCloud.py**: Defines a class for representing and manipulating point clouds.
8. **deleteFAIRObject.py**: Manages the deletion of resources from a server.
9. **F500.py**: Core class for processing F500 PlantEye data, including restructuring, point cloud processing, and data upload.
10. **processPointClouds.py**: Contains functions for writing histograms and calculating greenness indices.
11. **Fairdom.py**: Manages the creation and upload of data to the FAIRDOM platform.
12. **F500Azure.py**: Extends the F500 class for Azure Blob Storage interactions.
13. **fairdom.py**: Provides utility functions for handling data matrices and measurements.
14. **toolkit.py**: Main function for executing the command-line interface for the F500 class.

## Key Modules to Focus On

As a new developer, you should focus on understanding the following key modules:

- **F500.py**: This is the core module for processing PlantEye data. It includes methods for restructuring data, processing point clouds, and uploading data.
- **PointCloud.py**: This module provides a class for handling point cloud data, including methods for calculating various indices and rendering images.
- **computePhenotypes.py**: Understanding this module will help you grasp how different phenotypic indices are calculated from point cloud data.
- **Fairdom.py**: This module is crucial if you will be working on data uploads to the FAIRDOM platform.

## Typical Development Workflow

1. **Setup**: Clone the repository from [GitHub](https://git.wur.nl/NPEC/analytics) and set up your development environment. Ensure you have all necessary dependencies installed.

2. **Understanding the Data**: Familiarize yourself with the point cloud data format and the specific attributes used in the project (e.g., wavelengths, colors, NIR values).

3. **Feature Development**: When adding new features or modifying existing ones, focus on the relevant module. For example, if you're working on data visualization, you might focus on `visualization_ply.py` or `animate_ply.py`.

4. **Testing**: Write unit tests for your code to ensure it works as expected. Pay attention to edge cases and potential exceptions, as noted in the documentation.

5. **Documentation**: Update the documentation to reflect any changes you make. This includes updating docstrings and any relevant markdown files.

6. **Code Review**: Submit your changes for code review. Engage with feedback to improve the quality of your code.

7. **Deployment**: Once your changes are approved, follow the deployment process to integrate your changes into the main codebase.

8. **Communication**: If you have questions or need assistance, reach out to Sven Warris at [sven.warris@wur.nl](mailto:sven.warris@wur.nl).

By following this guide, you'll be well-equipped to contribute effectively to the F500 Data Analytics project. Welcome aboard!