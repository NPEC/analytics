import argparse
import sys
import os
import pandas
import pprint
import json
from isatools.isajson import ISAJSONEncoder
import isatools
from isatools.model import *
from isatools import isatab
from isatools import isajson
import re
from collections import defaultdict
import logging
import shutil
import glob
import gzip
import tempfile
import numpy
from Fairdom import Fairdom
import matplotlib.cm
import datetime
import string

class F500:
    """
    A class to handle the processing of F500 PlantEye data, including restructuring, point cloud processing, histogram combination, and data upload.

    Attributes:
        description (defaultdict): A dictionary to store descriptions.
        columnsToDrop (list): A list of columns to drop from the data.
        ISA (dict): A dictionary to store ISA-related data.
        datamatrix (list): A list to store data matrix information.
        investigation (Investigation): An ISA investigation object.
        checkAssayName (re.Pattern): A regex pattern to check assay names.
        measurements (DataFrame): A DataFrame to store measurements.
        currentFile (str): The current file being processed.
        currentRoot (str): The current root directory being processed.
        command (str): The command to execute.
        assaysDone (set): A set to store completed assays.
        samples (dict): A dictionary to store sample objects.
    """

    description = defaultdict(str)
    columnsToDrop = []
    ISA = {}
    datamatrix = []
    investigation = None
    checkAssayName = re.compile(r"f[0-9]+")
    measurements = None
    currentFile = ""
    currentRoot = ""
    command = None
    assaysDone = None
    samples = None
    
    def __init__(self):        
        """
        Initializes the F500 object with default values and configurations.
        """
        # Some columns contain the wrong data, remove those:
        self.columnsToDrop = ["ndvi_aver","ndvi_bin0","ndvi_bin1","ndvi_bin2","ndvi_bin3","ndvi_bin4","ndvi_bin5",
                         "greenness_aver","greenness_bin0","greenness_bin1","greenness_bin2","greenness_bin3","greenness_bin4","greenness_bin5",
                         "hue_aver","hue_bin0","hue_bin1","hue_bin2","hue_bin3","hue_bin4","hue_bin5",
                         "npci_aver","npci_bin0","npci_bin1","npci_bin2","npci_bin3","npci_bin4","npci_bin5",
                         "psri_aver","psri_bin0","psri_bin1","psri_bin2","psri_bin3","psri_bin4","psri_bin5"]

        self.measurements = pandas.DataFrame()
        self.assaysDone = set()
        self.metadata = None
        self.samples = {}

    def commandLineInterface(self):
        """
        Sets up the command-line interface for the script, defining arguments and subcommands.
        """
        my_parser = argparse.ArgumentParser(description='F500 PlantEye data processing tool.')
        sub_parsers = my_parser.add_subparsers(dest="command")

        my_parser_restructure = sub_parsers.add_parser("restructure")
        my_parser_restructure.add_argument('--loglevel', help="Application log level (INFO/WARN/ERROR)", default="INFO")
        my_parser_restructure.add_argument('--logfile', help="Application log file")
        my_parser_restructure.add_argument('-i', '--investigationPath',
                               help='Directory for project data', default=".")
        
        # Add the arguments
        my_parser_restructure.add_argument('-t', '--technologyType',
                               help='Type of technology used for phenotyping', default="Imaging")
        my_parser_restructure.add_argument('-p', '--technologyPlatform',
                               help='Platform used for phenotyping', default="PlantEye")
        my_parser_restructure.add_argument('-s', '--sampleType',
                               help='Name of the basic sample type', default="Pot")
        my_parser_restructure.add_argument('-S', '--sampleTypeContainer',
                               help='Name of the container sample type', default="Plot")
        
        my_parser_restructure.add_argument('--source',
                               help='Source definition', default="Plant")
        my_parser_restructure.add_argument('--sourceContainer',
                               help='Source container definition', default="Plot")

        my_parser_restructure.add_argument('--copyPointcloud',
                               help='Copy point clouds to new location (True/False)', default="True")


        my_parser_restructure.add_argument('-d', '--datamatrix_file', required = True,
                               help='Project metadata file (csv)')
        my_parser_restructure.add_argument('-m', '--metadata', required = False, default = None,
                               help='NPEC metadata file of all projects (csv)')
        my_parser_restructure.add_argument('-I', '--id', required = False, default = "-1",
                               help='NPEC project ID as found in the NPEC metadata file.')
        
        my_parser_restructure.add_argument('-j', '--json', required = True,
                               help='Output ISA JSON file')
        my_parser_restructure.add_argument('-o', '--organism', required = False, default = "Viridiplantae",
                               help='Formal name of the organism, for example "Solanum tuberosum"')
        my_parser_restructure.add_argument('-T', '--taxon', default = "33090",
                                           help='NCBI taxon id of the organism, for example 4113 (the number after http://purl.bioontology.org/ontology/NCBITAXON/)', required = False)

        my_parser_restructure.add_argument('start', nargs="+",
                               help='Main directories with the F500 PlantEye raw data')

        my_parser_pointclouds = sub_parsers.add_parser("pointclouds")

        my_parser_pointclouds.add_argument('--loglevel', help="Application log level (INFO/WARN/ERROR)", default="INFO")
        my_parser_pointclouds.add_argument('--logfile', help="Application log file")
        my_parser_pointclouds.add_argument('-i', '--investigationPath',
                               help='Directory for project data', default=".")


        my_parser_pointclouds.add_argument('-b', '--bins', help="Number of bins in the wavelengths histograms. Default: 256", default=256)
        my_parser_pointclouds.add_argument('-j', '--json', required = True,
                               help='Input ISA JSON file')
        my_parser_pointclouds.add_argument('--trim', help="Trim point cloud a this Z-axis (points below will be removed)", default=0)
        my_parser_pointclouds.add_argument('--force', help="Force all data to be reprocessed", default=False)
        my_parser_pointclouds.add_argument('--image_width', help="Width of the top view png image", default=1920)
        my_parser_pointclouds.add_argument('--image_height', help="Height of the top view png image", default=1080)

        my_parser_histogram = sub_parsers.add_parser("histogram")

        my_parser_histogram.add_argument('--loglevel', help="Application log level (INFO/WARN/ERROR)", default="INFO")
        my_parser_histogram.add_argument('--logfile', help="Application log file")
        my_parser_histogram.add_argument('-i', '--investigationPath',
                               help='Directory for project data', default=".")
        my_parser_histogram.add_argument('-j', '--json', required = True,
                               help='Input ISA JSON file')
        my_parser_histogram.add_argument('-H', '--histogram', required = True,
                               help='name of the histogram (eg. ndvi, greenness, hue or npci)')

        my_parser_upload = sub_parsers.add_parser("upload")

        my_parser_upload.add_argument('--loglevel', help="Application log level (INFO/WARN/ERROR)", default="INFO")
        my_parser_upload.add_argument('--logfile', help="Application log file")
        my_parser_upload.add_argument('-i', '--investigationPath',
                               help='Directory for project data', default=".")
        my_parser_upload.add_argument('-j', '--json', required = True,
                               help='Input ISA JSON file')
        my_parser_upload.add_argument('-t', '--token', required = True,
                               help='Access token for API')
        my_parser_upload.add_argument('-u', '--URL', required = True,
                               help='API URL')
        my_parser_upload.add_argument('-o', '--organism', required = True,
                               help='Formal name of the organism, for example "Solanum tuberosum"')
        my_parser_upload.add_argument('-p', '--project', required = True,
                               help='Project ID in the FAIRDOM-seek platform')
        my_parser_upload.add_argument('-s', '--sample_type', required = True,
                               help='Sample_type ID in the FAIRDOM-seek platform')

             
        # Execute the parse_args() method
        self.args = my_parser.parse_args()
                
        #if investigationPath in self.args:
        self.investigationPath = self.args.investigationPath
        #else:
        #    self.investigationPath = "."
        self.setLogger()
        
        
    def setLogger(self):
        """
        Configures the logging for the script based on command-line arguments.
        """
        self.logger = logging.getLogger("F500")
        self.logger.setLevel(self.args.loglevel)
        if len(str(self.args.logfile)) > 0: 
            self.logger.addHandler(logging.FileHandler(str(self.args.logfile)))
        
    @staticmethod
    def removeAfterSpaceFromDataMatrix(row):
        """
        Cleans up the 'DataMatrix' column in a DataFrame row by removing text after a space.

        Args:
            row (Series): A row from a DataFrame.

        Returns:
            Series: The modified row with cleaned 'DataMatrix' column.
        """
        try:
            row["DataMatrix"] = row["DataMatrix"].strip().split(" ")[0]
        except:
            pass
        return row

    def createISA(self):
        """
        Initializes an ISA investigation object and sets up the study and metadata.
        """
        # Create investigation
        self.investigation = Investigation()
        self.investigation.title = "_".join([self.datamatrix[4], self.datamatrix[3], self.datamatrix[2]])
        self.investigation.filename = self.datamatrix_file
        self.investigation.measurements = pandas.DataFrame()
        self.investigation.plots = set()

        # Create study, title comes datamatrix file (ID...)   
        self.investigation.studies.append(Study())
        if self.studyName != None:
            self.investigation.studies[0].title =  self.studyName
        else:
            self.investigation.studies[0].title =  self.datamatrix[3]
        self.investigation.studies[0].sources.append(self.source)
        self.investigation.studies[0].sources.append(self.sourceContainer)
        self.investigation.add_ontology_source_reference(name='NCBITaxon', description="NCBI Taxonomy")

        #store metadata file
        os.makedirs("/".join([self.investigationPath, self.investigation.title]), exist_ok=True)
        os.makedirs("/".join([self.investigationPath, self.investigation.title, self.investigation.studies[0].title, "metadata"]), exist_ok=True)
        
        self.metadata.to_csv("/".join([self.investigationPath, self.investigation.title, self.investigation.studies[0].title, "metadata", os.path.basename(self.datamatrix_file)]), sep=";", index=False)

        
    def writeISAJSON(self):
        """
        Writes the ISA investigation object to a JSON file.
        """
        jsonOutput = open(self.args.json, "w")
        jsonOutput.write(json.dumps(self.investigation, cls=ISAJSONEncoder, sort_keys=True, indent=4, separators=(',', ': ')))
        jsonOutput.close()


    @staticmethod
    def copyPots(row, pots, f500):
        """
        Copies pot information from a reference DataFrame to a row.

        Args:
            row (Series): A row from a DataFrame.
            pots (DataFrame): A DataFrame containing pot information.
            f500 (F500): An instance of the F500 class.

        Returns:
            Series: The modified row with pot information.
        """
        try:
            row["Pot"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Pot"].iloc[0]
            if "Treatment" in pots.columns:
                row["Treatment"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Treatment"].iloc[0]
            if "Experimemt" in pots.columns:
                row["Experiment"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Experiment"].iloc[0]
        except:
            f500.logger.warn("No pot found at [{},{}]".format(row["x"], row["y"]))
            row["Pot"] = "UNAVAILABLE WHILE PROCESSING"
        return row

    def measurementsToFile(self):
        """
        Writes the measurements DataFrame to a file.
        """
        path =  "/".join([self.investigationPath, self.investigation.title, self.investigation.studies[0].title])
        filename = "derived/" + self.investigation.studies[0].title + ".csv"
        os.makedirs(path + "/derived", exist_ok=True)
        self.investigation.measurements.to_csv(path + "/" + filename, sep=";", index=False)

    @staticmethod
    def rawMeasurementsToFile(path, filename, measurements):
        """
        Writes raw measurements to a file.

        Args:
            path (str): The directory path to save the file.
            filename (str): The name of the file.
            measurements (list): A list of measurements to write.
        """
        os.makedirs(path + "/derived", exist_ok=True)
        df = pandas.DataFrame(measurements)
        df = df.transpose()
        df.to_csv(path + "/" + filename, sep=";", index=False)

    @staticmethod
    def addPointClouds(row, title):
        """
        Adds point cloud file names to a row.

        Args:
            row (Series): A row from a DataFrame.
            title (str): The title to use in the file names.

        Returns:
            Series: The modified row with point cloud file names.
        """
        filename = "{}_{}_full_sx{:03d}_sy{:03d}.ply.gz".format(
            title, row["timestamp_file"], 
            int(row["x"]),
            int(row["y"]))
        row["pointcloud_full"] = filename
        filename = "{}_{}_sl_sx{:03d}_sy{:03d}.ply.gz".format(
            title, row["timestamp_file"], 
            int(row["x"]),
            int(row["y"]))
        row["pointcloud_sl"] = filename
        filename = "{}_{}_mr_sx{:03d}_sy{:03d}.ply.gz".format(
            title, row["timestamp_file"], 
            int(row["x"]),
            int(row["y"]))
        row["pointcloud_mr"] = filename

        filename = "{}_{}_mg_sx{:03d}_sy{:03d}.ply.gz".format(
            title, row["timestamp_file"], 
            int(row["x"]),
            int(row["y"]))
        row["pointcloud_mg"] = filename

        return row
    
    @staticmethod
    def copyPointcloudFile(row, f500, fullPath):
        """
        Copies point cloud files to a specified location.

        Args:
            row (Series): A row from a DataFrame.
            f500 (F500): An instance of the F500 class.
            fullPath (str): The destination path for the point cloud files.
        """
        if f500.args.copyPointcloud == "True": 
            AB = f500.root.split("/")[-1]
            pointcloudPath = "/".join(f500.root.split("/")[:-3]) + "/current/" + AB +'/I/'
            f500.logger.info("Copying pointclouds from {}{} to {}".format(pointcloudPath, [row["pointcloud_full"],row["pointcloud_mr"],row["pointcloud_sl"],row["pointcloud_mg"]], fullPath))

            try: 
                os.makedirs(fullPath, exist_ok=True)
            except Exception as e:
                f500.logger.warn("Exception in creating directories:\n{}".format(e))

            try:
                if not os.path.exists(fullPath + "/" + row["pointcloud_full"]):
                    shutil.copy(pointcloudPath+row["pointcloud_full"], fullPath) 
            except Exception as e:
                f500.logger.warn("Exception in copying file:\n{}".format(e))

            try:
                if not os.path.exists(fullPath + "/" + row["pointcloud_mr"]):
                    shutil.copy(pointcloudPath+row["pointcloud_mr"], fullPath) 
            except Exception as e:
                f500.logger.warn("Exception in copying file:\n{}".format(e))

            try:
                if not os.path.exists(fullPath + "/" + row["pointcloud_sl"]):
                    shutil.copy(pointcloudPath+row["pointcloud_sl"], fullPath) 
            except Exception as e:
                f500.logger.warn("Exception in copying file:\n{}".format(e))

            try:
                if not os.path.exists(fullPath + "/" + row["pointcloud_mg"]):
                    shutil.copy(pointcloudPath+row["pointcloud_mg"], fullPath) 
            except Exception as e:
                f500.logger.warn("Exception in copying file:\n{}".format(e))


        else:
            f500.logger.info("Skipping copy (defined in command line)")

    @staticmethod
    def copyPlotPointcloudFile(row, f500, fullPath, title):
        """
        Copies plot point cloud files to a specified location.

        Args:
            row (Series): A row from a DataFrame.
            f500 (F500): An instance of the F500 class.
            fullPath (str): The destination path for the plot point cloud files.
            title (str): The title to use in the file names.
        """
        if f500.args.copyPointcloud == "True": 
            f500.logger.warn("The copy plot point cloud will copy a lot of data. However, users are generally not interested in these plot files.")

            AB = f500.root.split("/")[-1]
            pointcloudPath = "/".join(f500.root.split("/")[:-3]) + "/current/" + AB 
            f500.logger.info("Files have timestamps, which can be off by a few milliseconds. Plot pointclouds will be copied using a wildcard for the last millisecond!")
            try:
                f500.logger.warn("Making dir: {}".format(fullPath))                
                os.makedirs(fullPath, exist_ok=True)
                timestamp_file = row["timestamp_file"].split("T")[0] + "*"
                timestamp = row["timestamp_file"]
                
                for MS in ['M', 'S']:
                    # Initialize variables to keep track of the closest timestamp and filename
                    closest_timestamp = None
                    closest_filename = None
    
                    # Parse the target timestamp
                    target_datetime = datetime.datetime.strptime(timestamp, "%Y%m%dT%H%M%S")
    
                    # Iterate through the list of filenames
                    for filename in glob.glob(pointcloudPath+"/{}/{}_{}.ply".format(MS, title, timestamp_file)):
                        # Get the filename without the path
                        filename_without_path = os.path.basename(full_file_path)
                        
                        # Get the filename without the extension
                        filename_without_extension = os.path.splitext(filename_without_path)[0]
                    # Extract the timestamp part of the filename
                        parts = filename_without_extension.split("_")
                        if len(parts) == 2:
                            file_timestamp = parts[1]
                    
                            # Parse the file timestamp
                            file_datetime = datetime.datetime.strptime(file_timestamp, "%Y%m%dT%H%M%S")
                    
                            # Calculate the time difference
                            time_difference = abs(target_datetime - file_datetime)
                    
                            # Check if this is the closest timestamp found so far
                            if closest_timestamp is None or time_difference < closest_timestamp:
                                closest_timestamp = time_difference
                                closest_filename = filename
    
                    # The closest_filename variable now holds the filename with the closest timestamp
                    if closest_filename:
                        print("Closest file:", closest_filename)
                        f500.logger.info("Copying pointclouds {} to {}".format(closest_filename , fullPath))
                        shutil.copy(closest_filename, fullPath+ "/" + MS + "_" + title[:-3] + "000.ply") 
                    else:
                        f500.logger.info("No matching plot file found")

            except Exception as e:
                f500.logger.warn("Exception in copying files:\n{}".format(e)) 

        else:
            f500.logger.info("Skipping copy plot files (defined in command line)")
    

    @staticmethod
    def createSample(samples, name, source, organism, taxon, term_source):
        """
        Creates a sample object if it doesn't already exist.

        Args:
            samples (dict): A dictionary to store sample objects.
            name (str): The name of the sample.
            source (Source): The source object for the sample.
            organism (str): The organism name.
            taxon (str): The taxon ID.
            term_source (OntologySourceReference): The ontology source reference.

        Returns:
            Sample: The created or existing sample object.
        """
        if str(name) not in samples:
            sample = Sample(name=str(name), derives_from=[source])
            characteristic_organism = Characteristic(category=OntologyAnnotation(term="Organism"),
                                             value=OntologyAnnotation(term=organism, term_source=term_source,
                                                                      term_accession="http://purl.bioontology.org/ontology/NCBITAXON/{}".format(taxon)))
            sample.characteristics.append(characteristic_organism)
            samples[str(name)] = sample
        else:
            sample = samples[str(name)]
        return sample

    @staticmethod
    def createAssay(row, f500, path, source):
        """
        Creates an assay object and adds it to the investigation.

        Args:
            row (Series): A row from a DataFrame.
            f500 (F500): An instance of the F500 class.
            path (str): The directory path for the assay.
            source (Source): The source object for the assay.
        """
        assay = Assay()
        assay.title = row["timestamp_file"]
        assay.filename = row["timestamp_file"]
        assay.technology_platform =  f500.args.technologyPlatform
        assay.technology_type = OntologyAnnotation(term="Imaging", term_accession="http://jermontology.org/ontology/JERMOntology#Imaging")
        sample = F500.createSample(f500.samples, row["Pot"], source, f500.args.organism, f500.args.taxon, f500.investigation.get_ontology_source_reference("NCBITaxon"))
    
        assay.add_comment("root", f500.root)
        assay.add_comment("file", f500.file)
    
        assay.samples.append(sample)
        assay.characteristic_categories.append(sample.characteristics[-1].category)
        
        #f500.investigation.studies[0].characteristic_categories.append(sample.characteristics[-1].category)
        
        #f500.investigation.studies[0].samples.append(sample)
        
        filename = "derived/" + assay.title + ".csv"
        fullPath = "/".join([f500.investigation.title, f500.investigation.studies[0].title, 
                             f500.args.sampleType, sample.name, assay.title, f500.args.technologyType, f500.args.technologyPlatform])
        dataFile = DataFile(filename=filename, label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/" + filename)
        assay.data_files.append(dataFile)
    
        F500.rawMeasurementsToFile(path + "/" + fullPath , filename, row)
        
        dataFile = DataFile(filename=row["pointcloud_full"], label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_full"])
        assay.data_files.append(dataFile)
        dataFile = DataFile(filename=row["pointcloud_sl"], label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_sl"])
        assay.data_files.append(dataFile)
        dataFile = DataFile(filename=row["pointcloud_mr"], label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_mr"])
        assay.data_files.append(dataFile)
        dataFile = DataFile(filename=row["pointcloud_mg"], label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_mr"])
        assay.data_files.append(dataFile)

        dataFile = DataFile(filename=row["pointcloud_full"] + ".png", label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_full"] + ".png")
        dataFile.add_comment("description", "Top view image")
        assay.data_files.append(dataFile)
        dataFile = DataFile(filename=row["pointcloud_full"] + ".ndvi.PNG", label="Image Data File")
        dataFile.add_comment("fullPath", fullPath + "/pointcloud/" + row["pointcloud_full"] + ".ndvi.PNG")
        dataFile.add_comment("description", "Top view image using the NDVI")
        assay.data_files.append(dataFile)
    
        F500.copyPointcloudFile(row, f500, path + "/" + fullPath + "/pointcloud/")
    
        dataFile = DataFile(filename=assay.title + "_ndvi.csv", label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/derived/" + assay.title + "_ndvi.csv")
        assay.data_files.append(dataFile)
        dataFile = DataFile(filename=assay.title + "_greenness.csv", label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/derived/" + assay.title + "_greenness.csv")
        assay.data_files.append(dataFile)
    
        dataFile = DataFile(filename=assay.title + "_npci.csv", label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/derived/" + assay.title + "_npci.csv")
        assay.data_files.append(dataFile)

        dataFile = DataFile(filename=assay.title + "_hue.csv", label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/derived/" + assay.title + "_hue.csv")
        assay.data_files.append(dataFile)

        dataFile = DataFile(filename=assay.title + "_psri.csv", label="Derived Data File")
        dataFile.add_comment("fullPath", fullPath + "/derived/" + assay.title + "_psri.csv")
        assay.data_files.append(dataFile)

    
        #dataFile = DataFile(filename=assay.title + ".png", label="Image Data File")
        #dataFile.add_comment("fullPath", fullPath + "/png/" + assay.title + ".png")
        #assay.data_files.append(dataFile)
    
        
        f500.investigation.studies[0].assays.append(assay)

    @staticmethod
    def createAssayPlot(row, f500, path, source, title):
        """
        Creates an assay plot object and adds it to the investigation.

        Args:
            row (Series): A row from a DataFrame.
            f500 (F500): An instance of the F500 class.
            path (str): The directory path for the assay plot.
            source (Source): The source object for the assay plot.
            title (str): The title to use in the file names.
        """
        assay = Assay()
        assay.title = row["timestamp_file"]
        assay.filename = row["timestamp_file"]
        assay.technology_platform = f500.args.technologyPlatform
        assay.technology_type = OntologyAnnotation(term="Imaging", term_accession="http://jermontology.org/ontology/JERMOntology#Imaging")
        sample = None
        if "Position" in row and str(row["Position"]) != "nan":
            sample = F500.createSample(f500.samples, row["Position"][:2], source, f500.args.organism, f500.args.taxon, f500.investigation.get_ontology_source_reference("NCBITaxon"))
        else:
            f500.logger.warn("Assay {} has no Position column, extracting Plot pointclouds not possible!".format(assay.title))
        if sample != None and ((assay.title, sample.name) not in f500.investigation.plots):
            f500.investigation.plots.add((assay.title, sample.name)) 
            
            assay.samples.append(sample)
            assay.characteristic_categories.append(sample.characteristics[-1].category)

            #f500.investigation.studies[0].samples.append(sample)
        
            filename = "S_" + assay.title[:-3] + "000.ply"
            fullPath = "/".join([f500.investigation.title, f500.investigation.studies[0].title, 
                                 f500.args.sampleTypeContainer, sample.name, assay.title, f500.args.technologyType, f500.args.technologyPlatform])
            dataFile = DataFile(filename=filename, label="Image Data File")
            dataFile.add_comment("fullPath", fullPath + "/" + filename)
            assay.data_files.append(dataFile)
            filename = "M_" + assay.title[:-3] + "000.ply"
            dataFile = DataFile(filename=filename, label="Image Data File")
            dataFile.add_comment("fullPath", fullPath + "/" + filename)
            assay.data_files.append(dataFile)
            F500.copyPlotPointcloudFile(row, f500, fullPath,assay.title)
            
            f500.investigation.studies[0].assays.append(assay)

    def correctDataMatrix(row, pots):
        """
        Corrects the 'DataMatrix' column in a row based on a reference DataFrame.

        Args:
            row (Series): A row from a DataFrame.
            pots (DataFrame): A DataFrame containing pot information.

        Returns:
            Series: The modified row with corrected 'DataMatrix' column.
        """
        result = pots.loc[(pots['x'] == row["x"]) & (pots['y'] == row['y']), 'Pot']
        
        # Access the result
        if len(result) == 1:
            value = result.iloc[0]  # Get the single unique value
            row['DataMatrix'] = value
        return row
            
    def finalize(self, title):
        """
        Finalizes the processing of measurements and creates assays.

        Args:
            title (str): The title to use in the file names.
        """
        # CSV will be combined data file (with corrected pot names) and ply file names
        # Do this, if the data matrix contains pot names (otherwise it either went wrong or data is from a different project
        # Then list the ply files as Image File
        if "Pot" in self.measurements.columns:
            pots = self.measurements.dropna(axis=0, subset=["Pot"])
            if len(pots) > 0 and "Pot" in pots.columns:
                #select rows that are not in self.metadata
                pots = pots[pots['Pot'].isin(self.metadata['DataMatrix'])]
                #self.logger.debug(pots['Pot'].unique())
                pots = pots.drop_duplicates(subset=["x","y"], keep="first")
                #self.logger.debug(pots['Pot'].unique())
                if len(pots) and pots.dtypes["Pot"] == self.metadata.dtypes["DataMatrix"] and len(self.metadata.merge(pots, how="inner", left_on="DataMatrix", right_on="Pot")) > 0:
                    self.logger.info("Found project data")
                    try:
                        # Perform a left merge to align C with A based on 'x' and 'y'
                        #merged_C = self.measurements.merge(pots[['x', 'y', 'DataMatrix']], on=['x', 'y'], how='left', suffixes=('', '_A'))
                        # Update 'DataMatrix' in C where there is a match in A
                        #self.measurements['Pot'] = merged_C['DataMatrix'].combine_first(self.measurements['Pot'])
                        self.measurements = self.measurements.apply(F500.correctDataMatrix, axis=1, pots = pots)
                        self.measurements = self.measurements.drop(self.columnsToDrop, axis=1, errors="ignore")
                        self.measurements = self.measurements.apply(F500.copyPots , axis=1, pots=pots, f500=self)
                        self.measurements = self.measurements.apply(F500.addPointClouds, axis=1, title=title)
                        #now create the assays, using the timestamp_file as name
                        self.measurements.apply(F500.createAssay, axis=1, f500=self, path = self.args.investigationPath, source=self.source )
                        self.investigation.measurements = pandas.concat([self.investigation.measurements, self.measurements], axis=0, ignore_index=True)
                    except Exception as e:
                        self.logger.warning("Could not process project data:\n{}".format(e))
                    assayPlot = self.measurements.merge(self.metadata, how="inner", left_on="Pot", right_on="DataMatrix")
                    if len(assayPlot) > 0 : 
                        assayPlot.apply(F500.createAssayPlot, axis=1, f500=self, path = self.args.investigationPath, source=self.sourceContainer, title=title)
                    else:
                        self.logger.warn("Unable to create assay plots {}".format(title))
                else:
                    self.logger.info("Data from different project")
            else:
                self.logger.info("No pots in columns file with valid information.")
        else:
            self.logger.info("No pots in main measurement file")

    def getDirectoryListing(self, rootFolder):
        """
        Returns a directory listing for a given root folder.

        Args:
            rootFolder (str): The root folder to list.

        Returns:
            generator: A generator yielding directory listings.
        """
        return os.walk(rootFolder)

    def restructure(self):
        """
        Restructures the raw data into an ISA-compliant format.
        """
        self.source = Source(name=self.args.source)
        self.sourceContainer = Source(name=self.args.sourceContainer)
        self.datamatrix = os.path.basename(self.args.datamatrix_file).split(".")[0].split("_")
        self.datamatrix_file = self.args.datamatrix_file
        self.studyName = None
        if self.metadata == None:
            self.metadata = pandas.read_csv(self.datamatrix_file, sep=";")
            self.metadata = self.metadata.apply(F500.removeAfterSpaceFromDataMatrix , axis=1)
        if self.args.metadata != None:
            if int(self.args.id) < 0:
                self.logger.error("NPEC project ID not defined. Required when providing the NPEC metadata file {}".format(self.args.metadata))
                exit(1)
            self.NPEC = pandas.read_csv(self.args.metadata, sep=";")
            self.NPEC = self.NPEC[self.NPEC["_experiment_id"] == int(self.args.id)]
            if len(self.NPEC) == 0:
                self.logger.error("NPEC project ID {} not found in the NPEC metadata file {}".format(self.args.id, self.args.metadata))
                exit(1)
            if "," not in self.NPEC["crops"]:
                self.args.taxon = 33090 # TODO: get this from the metadata file
                self.args.organism = str(self.NPEC["crops"]).strip().lower()
            self.studyName = str(self.NPEC["_experiment_title"]).translate(str.maketrans("", "", string.punctuation + string.whitespace))
        self.start = self.args.start

        previousAssay = ""
        self.createISA()
        for start in self.start:
            self.logger.info("Processing raw data in {}".format(start))
            for root, dirs, files in self.getDirectoryListing(start):
                for file in sorted(files):
                    if ".csv" in file:
                        csv = os.path.join(root, file)
                        assayName = os.path.basename(csv).split("_")[0]
                        if self.checkAssayName.match(assayName) != None:
                            timestamp = os.path.basename(csv).split("_")[1]
                            if os.path.basename(csv) in self.assaysDone:
                                self.logger.warn("Duplicate assay, skipping {}.".format(os.path.basename(csv)))
                            else:
                                self.assaysDone.add(os.path.basename(csv))
                                try: 
                                    if previousAssay != assayName:
                                        if len(self.measurements) > 0:
                                            # new assay, process all
                                            self.finalize(previousAssay)
                                        self.measurements =  pandas.DataFrame()
                                    self.file = file
                                    self.root = root
                                        
                                    self.logger.info("Reading: {}".format(csv))
                                    self.logger.info("Timestamp: {}".format(timestamp))
                                    try: 
                                        currentMeasurements = pandas.read_csv(csv, sep="\t", skiprows=[1])
                                        currentMeasurements["timestamp_file"] = timestamp
                                        # same assay
                                        if len(self.measurements) == 0:
                                            self.measurements = currentMeasurements
                                        else:
                                            self.measurements = pandas.concat([self.measurements, currentMeasurements], axis=0, ignore_index=True)
                                    except:
                                        self.logger.warn("No data in file: {}".format(csv))
                                    previousAssay = assayName
                                except Exception as e:
                                    self.logger.warning("Exception in reading data file {}:\n {}".format(csv, e))
                                    if self.args.loglevel == "DEBUG":
                                        raise e
                                    
                        else:
                            self.logger.info("CSV file {} is not a raw F500 file.".format(csv))
        
        if len(self.measurements) > 0:
            try: 
                self.finalize(previousAssay)
            except Exception as e:
                self.logger.warning("Exception in finalizing file:\n {}".format(e))
                if self.args.loglevel == "DEBUG":
                    raise e  

        self.investigation.studies[0].samples = list(self.samples.values())
        self.measurementsToFile()
        self.writeISAJSON()
        
    
    def processPointclouds(self):
        """
        Processes point cloud files and generates derived data.
        """
        from PointCloud import PointCloud

        self.logger.info("Reading project ISA {}".format(self.args.json))
        self.investigation = isajson.load(open(self.args.json, "r"))
        study = self.investigation.studies[0]
        self.args.force = self.args.force in ["True","true"]
        for assay in study.assays:
            self.sampleName = assay.samples[0].name
            self.timepoint = assay.filename
            self.logger.info("Processing assay {} with sample {}".format(assay.filename, self.sampleName))
            for df in assay.data_files:
                for com in df.comments:
                    if com.name == "fullPath" and "full" in com.value and com.value.endswith("ply.gz"):
                        self.logger.info("Pointcloud: {}".format(df.filename))
                        ply = self.args.investigationPath  + "/" + com.value
                        try:
                            outputBase = self.args.investigationPath + "/" + "/".join(com.value.split("/")[:-2]) + "/derived/" + assay.filename
                            if ((not os.path.exists(outputBase + "_greenness.csv")) or 
                                (not os.path.exists(outputBase + "_ndvi.csv")) or
                                (not os.path.exists(outputBase + "_npci.csv")) or
                                (not os.path.exists(outputBase + "_psri.csv")) or
                                (not os.path.exists(outputBase + ".png")) or
                                (not os.path.exists(outputBase + ".ndvi.PNG")) or                                
                                (not os.path.exists(outputBase + "_hue.csv")) or self.args.force):  
                                ply_raw = gzip.open(ply, "rb")
                                tmpFile = tempfile.NamedTemporaryFile()
                                tmpFile.write(ply_raw.read())
                                pcd = PointCloud(tmpFile.name)
                                pcd.trim(int(self.args.trim))
                                if (not os.path.exists(outputBase + "_greenness.csv") or self.args.force): 
                                    self.logger.info("Writing greenness histogram to {}".format(outputBase + "_greenness.csv"))
                                    greenness = pcd.get_greenness()
                                    pcd.writeHistogram(greenness, outputBase + "_greenness.csv", self.timepoint, self.sampleName, int(self.args.bins), dataRange=[-1,1])
                                if (not os.path.exists(outputBase + "_ndvi.csv") or self.args.force):
                                    self.logger.info("Writing ndvi histogram to {}".format(outputBase + "_ndvi.csv"))
                                    ndvi = pcd.get_ndvi()
                                    pcd.writeHistogram(ndvi, outputBase + "_ndvi.csv", self.timepoint, self.sampleName, int(self.args.bins), dataRange=[-1,1])
                                if (not os.path.exists(outputBase + "_npci.csv") or self.args.force):
                                    self.logger.info("Writing npci histogram to {}".format(outputBase + "_npci.csv"))
                                    npci = pcd.get_npci()
                                    pcd.writeHistogram(npci, outputBase + "_npci.csv", self.timepoint, self.sampleName, int(self.args.bins), dataRange=[-1,1])

                                if (not os.path.exists(outputBase + "_psri.csv") or self.args.force):
                                    self.logger.info("Writing psri histogram to {}".format(outputBase + "_psri.csv"))
                                    psri = pcd.get_psri()
                                    pcd.writeHistogram(psri, outputBase + "_psri.csv", self.timepoint, self.sampleName, int(self.args.bins), dataRange=[-1,1])
                                if (not os.path.exists(outputBase + "_hue.csv") or self.args.force):
                                    self.logger.info("Writing hue histogram to {}".format(outputBase + "_hue.csv"))
                                    hue = pcd.get_hue()
                                    pcd.writeHistogram(hue, outputBase + "_hue.csv", self.timepoint, self.sampleName, int(self.args.bins), dataRange=[0,360])
                                if (not os.path.exists(outputBase + ".png") or self.args.force):
                                    self.logger.info("Writing PNG of view at {}".format(self.args.investigationPath + "/" + com.value + ".png"))
                                    pcd.render_image(self.args.investigationPath + "/" + com.value+ ".png", self.args.image_width, self.args.image_height)
                                if (not os.path.exists(outputBase + ".ndvi.PNG" or self.args.force)):
                                    self.logger.info("Writing NDVI PNG of view at {}".format(self.args.investigationPath + "/" + com.value + ".ndvi.PNG"))
                                    if pcd.trimmed:
                                        pcd.pcd = pcd.untrimmedPCD
                                    
                                    pcd.pcd.computeNDVI()
                                    cmap = matplotlib.cm.get_cmap('rainbow')
                                    colors = numpy.asarray(pcd.pcd.ndvi)
                                    colors = cmap(colors[:,0])
                                    colors = colors[:, :3]
                                    pcd.setColors(colors)
                                    pcd.trim(int(self.args.trim))
                                    pcd.render_image(self.args.investigationPath + "/" + com.value+ ".ndvi.PNG", self.args.image_width, self.args.image_height, rescale=False)
                                
                                tmpFile.close()
                        except Exception as e:
                            self.logger.warning("Could not process point cloud: {}".format(e))


    def combineHistograms(self):
        """
        Combines histogram data from multiple assays into a single file.
        """
        self.logger.info("Reading project ISA {}".format(self.args.json))
        self.logger.info("Creating combined histogram of {}".format(self.args.histogram))
        self.investigation = isajson.load(open(self.args.json, "r"))
        study = self.investigation.studies[0]
        histogramLabels = self.args.histogram.split(",")
        for hLabel in histogramLabels:
            histogram = pandas.DataFrame()
            histogramList = []
            for assay in study.assays:
                self.sampleName = assay.samples[0].name
                self.timepoint = assay.filename
                self.logger.info("Processing assay {} with sample {}".format(assay.filename, self.sampleName))
                for df in assay.data_files:
                    for com in df.comments:
                        if com.name == "fullPath" and "_" + hLabel + ".csv" in com.value:
                            self.logger.info("histogram: {}".format(df.filename))
                            histogramFile = self.args.investigationPath  + "/" + com.value
                            try:
                                histogramData = pandas.read_csv(histogramFile, sep=";")
                                if len(histogramList) > 0:
                                    histogramData = histogramData[histogramData["sample"] != "edges"]
                                histogramList.append(histogramData) 
                            except Exception as e:
                                self.logger.warning("Could not process histogram file: {}".format(e))
            try:
                histogramFilename = "/".join([self.args.investigationPath,
                                              self.investigation.title,
                                              study.title, 
                                              "derived",
                                              study.title + "_" + hLabel + ".csv"])
                self.logger.info("Combining histograms and writing it to file {}".format(histogramFilename))
                histogram = pandas.concat(histogramList, axis=0, ignore_index=True)
                histogram.to_csv(histogramFilename, sep=";", index=False)
            except Exception as e:
                self.logger.warning("Could not combine data for {}, exception: {}".format(hLabel, e))
                
    def upload(self):
        """
        Uploads the processed data to a specified platform.
        """
        self.logger.info("Reading project ISA {}".format(self.args.json))
        self.logger.info("Uploading data to {}".format(self.args.URL))
        self.investigation = isajson.load(open(self.args.json, "r"))
        fairdom = Fairdom(self.investigation, self.args, self.logger)
        fairdom.upload()
        