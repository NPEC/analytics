from azure.storage.blob import BlobServiceClient
from F500 import F500
import os
import json
import pandas
import shutil

"""
This script provides functionality to interact with Azure Blob Storage for managing 
and processing data related to plant imaging experiments. It extends the F500 class 
to include methods for initializing Azure connections, transferring data, and handling 
experiment metadata.
"""

class F500Azure(F500):
    """
    A class to manage Azure Blob Storage interactions for plant imaging experiments.

    This class extends the F500 class and provides additional methods to initialize 
    Azure connections, transfer data between source and target containers, and handle 
    experiment metadata.
    """

    def __init__(self, experimentID):
        """
        Initialize the F500Azure class with a specific experiment ID.

        Args:
            experimentID (str): The unique identifier for the experiment.
        """
        super().__init__()
        self.experimentID = experimentID

    def initAzure(self, environment, metadata, logger):
        """
        Initialize Azure-related settings and metadata for the experiment.

        Args:
            environment (dict): A dictionary containing environment-specific settings.
            metadata (dict): Metadata related to the experiment.
            logger (Logger): Logger instance for logging information.

        Side Effects:
            Sets various attributes related to the experiment and Azure configuration.
        """
        self.logger = logger
        self.args.technologyType = "Imaging"
        self.args.technologyPlatform = "PlantEye"
        self.args.sampleType = "Pot"
        self.args.sampleTypeContainer = "Plot"
        self.args.source = "Plant"
        self.args.sourceContainer = "Plot"
        self.args.copyPointcloud = "True"
        self.args.investigationPath = str(self.experimentID)

        self.args.datamatrix_file = environment["datamatrix_file"]
        self.args.json = environment["json"]
        self.args.organism = environment["organism"]
        self.args.taxon = environment["taxon"]
        self.args.start = environment["start"]
        self.metadata = metadata

    def connectToSource(self, sourceConnectionString, sourceContainerName, sourceBlobName):
        """
        Connect to the source Azure Blob Storage container.

        Args:
            sourceConnectionString (str): Connection string for the source Azure Blob Storage.
            sourceContainerName (str): Name of the source container.
            sourceBlobName (str): Name of the source blob.

        Side Effects:
            Initializes the source blob service and container clients.
        """
        self.sourceConnectionString = sourceConnectionString
        self.sourceContainerName = sourceContainerName
        self.sourceBlobName = sourceBlobName
        self.sourceBlobServiceClient = BlobServiceClient.from_connection_string(sourceConnectionString)
        self.sourceContainerClient = self.sourceBlobServiceClient.get_container_client(sourceContainerName)

    def connectToTarget(self, targetConnectionString, targetContainerName, targetBlobName):
        """
        Connect to the target Azure Blob Storage container.

        Args:
            targetConnectionString (str): Connection string for the target Azure Blob Storage.
            targetContainerName (str): Name of the target container.
            targetBlobName (str): Name of the target blob.

        Side Effects:
            Initializes the target blob service and container clients.
        """
        self.targetConnectionString = targetConnectionString
        self.targetContainerName = targetContainerName
        self.targetBlobName = targetBlobName
        self.targetBlobServiceClient = BlobServiceClient.from_connection_string(targetConnectionString)
        self.targetContainerClient = self.targetBlobServiceClient.get_container_client(targetContainerName)

    def writeISAJSON(self):
        """
        Write the investigation data to a JSON file.

        Side Effects:
            Creates a JSON file with the investigation data.
        """
        jsonOutput = open(self.args.json, "w")
        jsonOutput.write(json.dumps(self.investigation, cls=ISAJSONEncoder, sort_keys=True, indent=4, separators=(',', ': ')))
        jsonOutput.close()

    def measurementsToFile(self):
        """
        Write the measurements data to a CSV file.

        Side Effects:
            Creates directories and a CSV file with the measurements data.
        """
        path = "/".join([self.investigationPath, self.investigation.title, self.investigation.studies[0].title])
        filename = "derived/" + self.investigation.studies[0].title + ".csv"
        os.makedirs(path + "/derived", exist_ok=True)
        self.investigation.measurements.to_csv(path + "/" + filename, sep=";")

    @staticmethod
    def rawMeasurementsToFile(path, filename, measurements):
        """
        Write raw measurements data to a CSV file.

        Args:
            path (str): The directory path where the file will be saved.
            filename (str): The name of the file.
            measurements (dict): The measurements data to be written.

        Side Effects:
            Creates directories and a CSV file with the raw measurements data.
        """
        os.makedirs(path + "/derived", exist_ok=True)
        df = pandas.DataFrame(measurements)
        df = df.transpose()
        df.to_csv(path + "/" + filename, sep=";")

    @staticmethod
    def copyPointcloudFile(row, f500, fullPath):
        """
        Copy pointcloud files to a specified directory.

        Args:
            row (dict): A dictionary containing pointcloud file names.
            f500 (F500): An instance of the F500 class.
            fullPath (str): The destination directory path.

        Side Effects:
            Copies pointcloud files to the specified directory.

        Exceptions:
            Raises an exception if file copying fails.
        """
        if f500.args.copyPointcloud == "True":
            AB = f500.root.split("/")[-1]
            pointcloudPath = "/".join(f500.root.split("/")[:-3]) + "/current/" + AB + '/I/'
            f500.logger.info("Copying pointclouds from {}{} to {}".format(pointcloudPath, [row["pointcloud_full"], row["pointcloud_mr"], row["pointcloud_sl"], row["pointcloud_mg"]], fullPath))
            try:
                os.makedirs(fullPath, exist_ok=True)
                shutil.copy(pointcloudPath + row["pointcloud_full"], fullPath)
                shutil.copy(pointcloudPath + row["pointcloud_mr"], fullPath)
                shutil.copy(pointcloudPath + row["pointcloud_sl"], fullPath)
                shutil.copy(pointcloudPath + row["pointcloud_mg"], fullPath)
            except Exception as e:
                f500.logger.warn("Exception in copying files:\n{}".format(e))
                # if f500.args.loglevel == "DEBUG":
                #     raise e
        else:
            f500.logger.info("Skipping copy (defined in command line)")

    @staticmethod
    def getDirectoryListing(rootFolder):
        """
        Get a directory listing for the specified root folder.

        Args:
            rootFolder (str): The root folder path.

        Returns:
            generator: A generator yielding directory paths, directory names, and file names.
        """
        return os.walk(rootFolder)