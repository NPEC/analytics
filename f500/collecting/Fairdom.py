from isatools.isajson import ISAJSONEncoder
import isatools
from isatools.model import *
from isatools import isatab
from isatools import isajson
import pandas
import requests
import json
import time


class Fairdom:
    """
    A class to manage the creation and upload of investigations, studies, assays, samples, and data files to the FAIRDOM platform.

    Attributes:
        investigation: An ISA-Tools investigation object containing the data to be uploaded.
        args: Command-line arguments or configuration settings for the upload process.
        logger: A logging object to record the process of uploading data.
        session: A requests session object configured with headers for authentication with the FAIRDOM API.
    """

    def __init__(self, investigation, args, logger):
        """
        Initializes the Fairdom class with the given investigation, arguments, and logger.

        Args:
            investigation: An ISA-Tools investigation object.
            args: An object containing command-line arguments or configuration settings.
            logger: A logging object for recording the upload process.

        Side Effects:
            Updates the session headers with authentication information.
        """
        self.investigation = investigation
        self.args = args
        self.args.project = int(self.args.project)
        self.args.organism = int(self.args.organism)
        self.logger = logger
        
        headers = {
            "Content-type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
            "Accept-Charset": "ISO-8859-1",
            "Authorization": "Token {}".format(self.args.token)
        }

        self.session = requests.Session()
        self.session.headers.update(headers)
    
    def createInvestigationJSON(self):
        """
        Creates a JSON structure for an investigation.

        Returns:
            A dictionary representing the JSON structure of the investigation.
        """
        investigationJSON = {}
        investigationJSON['data'] = {}
        investigationJSON['data']['type'] = 'investigations'
        investigationJSON['data']['attributes'] = {}
        investigationJSON['data']['attributes']['title'] = self.investigation.title
        investigationJSON['data']['attributes']['description'] = "PlantEye data from NPEC"
        investigationJSON['data']['relationships'] = {}
        investigationJSON['data']['relationships']['projects'] = {}
        investigationJSON['data']['relationships']['projects']['data'] = [{'id': str(self.args.project), 'type': 'projects'}]
        return investigationJSON

    def createStudyJSON(self, study, investigationID):
        """
        Creates a JSON structure for a study.

        Args:
            study: An ISA-Tools study object.
            investigationID: The ID of the investigation to which the study belongs.

        Returns:
            A dictionary representing the JSON structure of the study.
        """
        studyJSON = {}
        studyJSON['data'] = {}
        studyJSON['data']['type'] = 'studies'
        studyJSON['data']['attributes'] = {}
        studyJSON['data']['attributes']['title'] = study.name
        studyJSON['data']['attributes']['description'] = "F500 pot data"
        studyJSON['data']['relationships'] = {}
        studyJSON['data']['relationships']['investigation'] = {}
        studyJSON['data']['relationships']['investigation']['data'] = {'id': str(investigationID), 'type': 'investigations'}
        return studyJSON
    
    def createAssayJSON(self, assay, studyID):
        """
        Creates a JSON structure for an assay.

        Args:
            assay: An ISA-Tools assay object.
            studyID: The ID of the study to which the assay belongs.

        Returns:
            A dictionary representing the JSON structure of the assay.
        """
        assayJSON = {}
        assayJSON['data'] = {}
        assayJSON['data']['type'] = 'assays'
        assayJSON['data']['attributes'] = {}
        assayJSON['data']['attributes']['title'] = assay.filename
        assayJSON['data']['attributes']['description'] = 'NPEC F500 measurement assay'
        assayJSON['data']['attributes']['assay_class'] = {'key': 'EXP'}
        assayJSON['data']['attributes']['assay_type'] = {'uri': "http://jermontology.org/ontology/JERMOntology#Metabolomics"}
        assayJSON['data']['relationships'] = {}
        assayJSON['data']['relationships']['study'] = {}
        assayJSON['data']['relationships']['study']['data'] = {'id': str(studyID), 'type': 'studies'}
        assayJSON['data']['relationships']['organisms'] = {}
        assayJSON['data']['relationships']['organisms']['data'] = [{'id': str(self.args.organism), 'type': 'organisms'}]
        return assayJSON
    
    def createDataFileJSON(self, data_file):
        """
        Creates a JSON structure for a data file.

        Args:
            data_file: An object representing a data file.

        Returns:
            A dictionary representing the JSON structure of the data file.
        """
        data_fileJSON = {}
        data_fileJSON['data'] = {}
        data_fileJSON['data']['type'] = 'data_files'
        data_fileJSON['data']['attributes'] = {}
        data_fileJSON['data']['attributes']['title'] = data_file.filename
        data_fileJSON['data']['attributes']['content_blobs'] = [{
            'url': 'https://www.wur.nl/upload/854757ab-168f-46d7-b415-f8b501eebaa5_WUR_RGB_standard_2021-site.svg',
            'original_filename': data_file.filename,
            'content-type': 'image/svg+xml'
        }]
        data_fileJSON['data']['relationships'] = {}
        data_fileJSON['data']['relationships']['projects'] = {}
        data_fileJSON['data']['relationships']['projects']['data'] = [{'id': str(self.args.project), 'type': 'projects'}]
        return data_fileJSON
    
    def addSampleToAssayJSON(self, sampleID, assayJSON):
        """
        Adds a sample to an assay JSON structure.

        Args:
            sampleID: The ID of the sample to be added.
            assayJSON: The JSON structure of the assay to which the sample will be added.
        """
        if 'samples' not in assayJSON['data']['relationships']:
            assayJSON['data']['relationships']['samples'] = {}
            assayJSON['data']['relationships']['samples']['data'] = []
        assayJSON['data']['relationships']['samples']['data'].append({'id': str(sampleID), 'type': 'samples'})

    def addDataFileToAssayJSON(self, data_fileID, assayJSON):
        """
        Adds a data file to an assay JSON structure.

        Args:
            data_fileID: The ID of the data file to be added.
            assayJSON: The JSON structure of the assay to which the data file will be added.
        """
        if 'data_files' not in assayJSON['data']['relationships']:
            assayJSON['data']['relationships']['data_files'] = {}
            assayJSON['data']['relationships']['data_files']['data'] = []
        assayJSON['data']['relationships']['data_files']['data'].append({'id': str(data_fileID), 'type': 'data_files'})

    def addDataFilesToSampleJSON(self, assayJSON, sampleJSON):
        """
        Adds data files from an assay to a sample JSON structure.

        Args:
            assayJSON: The JSON structure of the assay containing the data files.
            sampleJSON: The JSON structure of the sample to which the data files will be added.
        """
        if 'data_files' not in sampleJSON['data']['relationships']:
            sampleJSON['data']['relationships']['data_files'] = {}
            sampleJSON['data']['relationships']['data_files']['data'] = []
        if 'data_files' in assayJSON['data']['relationships']:
            sampleJSON['data']['relationships']['data_files']['data'].extend(assayJSON['data']['relationships']['data_files']['data'])

    def createSampleJSON(self, sample):
        """
        Creates a JSON structure for a sample.

        Args:
            sample: An ISA-Tools sample object.

        Returns:
            A dictionary representing the JSON structure of the sample.
        """
        sampleJSON = {}
        sampleJSON['data'] = {}
        sampleJSON['data']['type'] = 'samples'
        sampleJSON['data']['attributes'] = {}
        sampleJSON['data']['attributes']['title'] = sample.name
        sampleJSON['data']['attributes']['attribute_map'] = {'PotID': sample.name}
        sampleJSON['data']['relationships'] = {}
        sampleJSON['data']['relationships']['projects'] = {}
        sampleJSON['data']['relationships']['projects']['data'] = [{'id': str(self.args.project), 'type': 'projects'}]
        sampleJSON['data']['relationships']['sample_type'] = {}
        sampleJSON['data']['relationships']['sample_type']['data'] = {'id': str(self.args.sample_type), 'type': 'sample_types'}
        return sampleJSON
    
    def upload(self):
        """
        Uploads the investigation, studies, assays, samples, and data files to the FAIRDOM platform.

        Side Effects:
            Communicates with the FAIRDOM API to create and upload data structures.
            Logs the process and any errors encountered.

        Raises:
            SystemExit: If an error occurs during the upload process that prevents continuation.
        """
        # create investigation
        investigationJSON = self.createInvestigationJSON()
        self.logger.info("Creating investigation in FAIRDOM at {}".format(self.args.URL))
        r = self.session.post(self.args.URL + '/investigations', json=investigationJSON)
        if r.status_code == 201 or r.status_code == 200:
            investigationID = r.json()['data']['id']
            self.logger.info("Investigation id {} created. Status: {}".format(investigationID, r.status_code))
        else:
            self.logger.error("Could not create new investigation, error code {}".format(r.status_code))
            exit(1)
    
        for study in self.investigation.studies:
            self.currentStudies = {}
            self.samples = {}
            countFiles = 0
            countAssays = 0
            for assay in study.assays:
                if countAssays > 10:
                    break
                else:
                    countAssays += 1
                self.logger.info("Processing assay {}".format(assay.filename))
                for sample in assay.samples:
                    if sample.name not in self.currentStudies:
                        self.currentStudies[sample.name] = self.createStudyJSON(sample, investigationID)
                    
                        r = self.session.post(self.args.URL + '/studies', json=self.currentStudies[sample.name])
                        if r.status_code == 201 or r.status_code == 200:
                            studyID = r.json()['data']['id']
                            self.currentStudies[sample.name]["id"] = studyID 
                            self.logger.info("Study id {} with ({}) created. Status: {}".format(studyID, sample.name, r.status_code))
                        else:
                            self.logger.error("Could not create new study, error code {}".format(r.status_code))
                            exit(1)

                    studyJSON = self.currentStudies[sample.name]
                assayJSON = self.createAssayJSON(assay, studyJSON['id'])
                # create add data files
                for data_file in assay.data_files:
                    if "derived" in data_file.filename or ".ply.gz" in data_file.filename or "ndvi" in data_file.filename:  # for now, only upload phenotypic data
                        data_fileJSON = self.createDataFileJSON(data_file)
                        r = self.session.post(self.args.URL + '/data_files', json=data_fileJSON)
                        if r.status_code == 201 or r.status_code == 200:
                            data_fileID = r.json()['data']['id']
                            self.logger.info("Data file id {} created ({}). Status: {}".format(data_fileID, data_file.filename, r.status_code))
                        else:
                            self.logger.error("Could not create new data file, error code {}".format(r.status_code))
                            exit(1)
                        data_fileJSON['id'] = data_fileID
                        self.addDataFileToAssayJSON(data_fileID, assayJSON)
                for sample in assay.samples:
                    if sample.name not in self.samples:
                        self.samples[sample.name] = self.createSampleJSON(sample)
                        r = self.session.post(self.args.URL + '/samples', json=self.samples[sample.name])
                        if r.status_code == 201 or r.status_code == 200:
                            sampleID = r.json()['data']['id']
                            self.samples[sample.name]['id'] = sampleID
                            self.logger.info("Sample id {} created ({}). Status: {}".format(sampleID, sample.name, r.status_code))
                        else:
                            self.logger.error("Could not create new sample, error code {}".format(r.status_code))
                            if r.status_code == 422:
                                self.logger.info(self.logger.info(self.samples[sample.name]))
                                self.logger.info(r.json())
                            exit(1)
                    sampleID = self.samples[sample.name]['id']
                    self.addSampleToAssayJSON(sampleID, assayJSON)
                    sampleJSON = self.samples[sample.name]
                r = self.session.post(self.args.URL + '/assays', json=assayJSON)
                if r.status_code == 201 or r.status_code == 200:
                    assayID = r.json()['data']['id']
                    self.logger.info("Assay id {} created. Status: {}".format(assayID, r.status_code))
                else:
                    self.logger.error("Could not create new assay, error code {}".format(r.status_code))
                    if r.status_code == 422:
                        self.logger.info(self.logger.info(assayJSON))
                        self.logger.info(r.json())
                        # try again
                        self.logger.info("Waiting to try again")
                        time.sleep(5)
                        r = self.session.post(self.args.URL + '/assays', json=assayJSON)
                        r.raise_for_status()
                    else:
                        exit(1)