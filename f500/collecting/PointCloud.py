import open3d as o3d
import numpy
import os

"""
This script provides a class for handling point cloud data using the Open3D library. 
It includes functionalities for reading point cloud data from a file, calculating various 
spectral indices, trimming the point cloud based on z-values, and rendering images of the 
point cloud with or without color rescaling.
"""

class PointCloud:
    """
    A class to represent and manipulate a point cloud using Open3D.

    Attributes:
    ----------
    pcd : open3d.geometry.PointCloud
        The point cloud data.
    trimmed : bool
        A flag indicating whether the point cloud has been trimmed.
    """

    pcd = None
    trimmed = False

    def __init__(self, filename):
        """
        Initializes the PointCloud object by reading point cloud data from a file.

        Parameters:
        ----------
        filename : str
            The path to the point cloud file in PLY format.
        """
        self.pcd = o3d.io.read_point_cloud(filename, format="ply")
        self.trimmed = False

    def writeHistogram(self, data, filename, timepoint, sampleName, bins, dataRange=None):
        """
        Writes a histogram of the given data to a file.

        Parameters:
        ----------
        data : numpy.ndarray
            The data for which the histogram is to be calculated.
        filename : str
            The path to the file where the histogram will be written.
        timepoint : str
            The timepoint associated with the data.
        sampleName : str
            The name of the sample.
        bins : int
            The number of bins for the histogram.
        dataRange : tuple, optional
            The lower and upper range of the bins. If not provided, range is (data.min(), data.max()).

        Side Effects:
        ------------
        Writes the histogram data to the specified file.
        """
        data = data[numpy.isfinite(data)]
        hist, bin_edges = numpy.histogram(data, bins=bins, range=dataRange)
        with open(filename, "w") as f:
            f.write("timepoint;sample;{}\n".format(";".join(["bin" + str(x) for x in range(0, len(bin_edges))])))
            f.write("{};{};{}\n".format(timepoint, "edges", ";".join([str(x) for x in bin_edges])))
            f.write("{};{};{}\n".format(timepoint, sampleName, ";".join([str(x) for x in hist])))

    def getWavelengths(self):
        """
        Retrieves the wavelengths from the point cloud.

        Returns:
        -------
        numpy.ndarray
            The wavelengths as a numpy array. If the point cloud is trimmed, returns a vertically stacked array.
        """
        if self.trimmed:
            return numpy.vstack(self.pcd.wavelengths)
        else:
            return numpy.asarray(self.pcd.wavelengths)

    def get_psri(self):
        """
        Calculates the Plant Senescence Reflectance Index (PSRI).

        Returns:
        -------
        numpy.ndarray
            The PSRI values calculated as (RED - GREEN) / NIR.
        """
        numpy.seterr(divide='ignore', invalid='ignore')
        wavelengths = self.getWavelengths()
        red = wavelengths[:, 0]
        green = wavelengths[:, 1]
        nir = wavelengths[:, 3]
        return ((red - green) / nir)

    def get_hue(self):
        """
        Calculates the hue from the RGB wavelengths.

        Returns:
        -------
        numpy.ndarray
            The hue values calculated from the RGB wavelengths.
        """
        numpy.seterr(divide='ignore', invalid='ignore')
        wavelengths = self.getWavelengths()
        red = wavelengths[:, 0]
        green = wavelengths[:, 1]
        blue = wavelengths[:, 2]
        hue = numpy.zeros(len(red))
        for c in range(len(hue)):
            minColor = min([red[c], green[c], blue[c]])
            maxColor = max([red[c], green[c], blue[c]])
            if minColor != maxColor:
                if maxColor == red[c]:
                    hue[c] = (green[c] - blue[c]) / (maxColor - minColor)
                elif maxColor == green[c]:
                    hue[c] = 2.0 + (blue[c] - red[c]) / (maxColor - minColor)
                else:
                    hue[c] = 4.0 + (red[c] - green[c]) / (maxColor - minColor)

                hue[c] = hue[c] * 60.0
                if hue[c] < 0:
                    hue[c] = hue[c] + 360.0
        return hue

    def get_greenness(self):
        """
        Calculates the greenness index.

        Returns:
        -------
        numpy.ndarray
            The greenness values calculated as (2*G - R - B) / (2*R + G + B).
        """
        numpy.seterr(divide='ignore', invalid='ignore')
        wavelengths = self.getWavelengths()
        return ((2.0 * wavelengths[:, 1] - wavelengths[:, 0] - wavelengths[:, 2]) /
                (2.0 * wavelengths[:, 1] + wavelengths[:, 0] + wavelengths[:, 2]))

    def get_ndvi(self):
        """
        Calculates the Normalized Difference Vegetation Index (NDVI).

        Returns:
        -------
        numpy.ndarray
            The NDVI values calculated as (NIR - RED) / (NIR + RED).
        """
        numpy.seterr(divide='ignore', invalid='ignore')
        wavelengths = self.getWavelengths()
        return (wavelengths[:, 3] - wavelengths[:, 0]) / (wavelengths[:, 3] + wavelengths[:, 0])

    def get_npci(self):
        """
        Calculates the Normalized Pigment Chlorophyll Index (NPCI).

        Returns:
        -------
        numpy.ndarray
            The NPCI values calculated as (RED - BLUE) / (RED + BLUE).
        """
        numpy.seterr(divide='ignore', invalid='ignore')
        wavelengths = self.getWavelengths()
        return ((wavelengths[:, 0] - wavelengths[:, 2]) / (wavelengths[:, 0] + wavelengths[:, 2]))

    def setColors(self, colors):
        """
        Sets the colors of the point cloud.

        Parameters:
        ----------
        colors : numpy.ndarray
            The colors to be set for the point cloud.
        """
        self.pcd.colors = o3d.utility.Vector3dVector(colors)

    def render_image(self, filename, image_width, image_height, rescale=True):
        """
        Renders an image of the point cloud.

        Parameters:
        ----------
        filename : str
            The path to the file where the image will be saved.
        image_width : int
            The width of the image.
        image_height : int
            The height of the image.
        rescale : bool, optional
            Whether to rescale the colors before rendering. Default is True.
        """
        if rescale:
            self.render_image_rescale(filename, image_width, image_height)
        else:
            self.render_image_no_rescale(filename, image_width, image_height)

    def trim(self, zIndex):
        """
        Trims the point cloud based on the z-values.

        Parameters:
        ----------
        zIndex : float
            The z-value threshold for trimming the point cloud.

        Side Effects:
        ------------
        Modifies the point cloud to only include points with z-values greater than or equal to zIndex.
        """
        if zIndex == 0:
            return
        self.untrimmedPCD = self.pcd
        points = numpy.asarray(self.pcd.points)
        mask = points[:, 2] >= zIndex
        filtered_points = points[mask]
        filtered_pcd = o3d.geometry.PointCloud()
        filtered_pcd.points = o3d.utility.Vector3dVector(filtered_points)

        if self.pcd.has_colors():
            colors = numpy.asarray(self.pcd.colors)
            filtered_colors = colors[mask]
            filtered_pcd.colors = o3d.utility.Vector3dVector(filtered_colors)

        wavelengths = numpy.asarray(self.pcd.wavelengths)
        filtered_wavelengths = wavelengths[mask]
        filtered_pcd.wavelengths = filtered_wavelengths
        self.pcd = filtered_pcd
        self.trimmed = True

    def render_image_no_rescale(self, filename, image_width, image_height):
        """
        Renders an image of the point cloud without rescaling the colors.

        Parameters:
        ----------
        filename : str
            The path to the file where the image will be saved.
        image_width : int
            The width of the image.
        image_height : int
            The height of the image.

        Side Effects:
        ------------
        Saves the rendered image to the specified file.
        """
        vis = o3d.visualization.Visualizer()
        vis.create_window(width=image_width, height=image_height, visible=False)
        vis.add_geometry(self.pcd)
        vis.update_geometry(self.pcd)
        vis.capture_screen_image(filename, do_render=True)
        vis.destroy_window()

    def render_image_rescale(self, filename, image_width, image_height):
        """
        Renders an image of the point cloud with rescaled colors.

        Parameters:
        ----------
        filename : str
            The path to the file where the image will be saved.
        image_width : int
            The width of the image.
        image_height : int
            The height of the image.

        Side Effects:
        ------------
        Saves the rendered image to the specified file.
        """
        colors = self.getWavelengths()
        colors = colors[:, :3]
        p01 = numpy.percentile(colors, 1)
        p99 = numpy.percentile(colors, 99)
        scaled_colors = ((colors - p01) / (p99 - p01) * 255)
        scaled_colors = numpy.clip(scaled_colors, 0, 255).astype(numpy.uint8)
        scaled_colors = scaled_colors.astype(numpy.float64) / 255

        if len(scaled_colors.shape) < 2:
            scaled_colors = numpy.reshape(scaled_colors, (-1, 3))

        self.pcd.colors = o3d.utility.Vector3dVector(scaled_colors)
        self.render_image_no_rescale(filename, image_width, image_height)