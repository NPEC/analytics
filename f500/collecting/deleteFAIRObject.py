"""
This script is designed to delete various types of resources from a specified host using the Fairdom SEEK API. 
It utilizes the requests library to send HTTP DELETE requests to remove data files, samples, assays, studies, 
and investigations from the server. The script requires an authorization token to authenticate the requests.

Usage:
    python script_name.py <token>

Where <token> is the authorization token required for accessing the API.

Note: This script performs destructive actions by deleting resources. Use with caution.
"""

import requests
import sys

def main():
    """Main function to execute the deletion of resources.

    This function sets up the session with the necessary headers and iterates over predefined ranges 
    to delete resources from the server. It deletes data files, samples, assays, studies, and investigations.

    Raises:
        requests.exceptions.RequestException: If a network-related error occurs during the requests.
    """
    token = sys.argv[1]

    headers = {
        "Content-type": "application/vnd.api+json",
        "Accept": "application/vnd.api+json",
        "Accept-Charset": "ISO-8859-1",
        "Authorization": "Token {}".format(token)
    }

    session = requests.Session()
    session.headers.update(headers)
    r = 32000
    host = "https://test.fairdom-seek.bif.containers.wurnet.nl/"

    # Delete data files
    for i in range(1000, r):
        session.delete(host + "data_files/{}".format(i))

    # Delete samples
    for i in range(0, 500):
        session.delete(host + "samples/{}".format(i))

    # Delete assays
    for i in range(0, 1300):
        session.delete(host + "assays/{}".format(i))

    # Delete studies
    for i in range(0, 50):
        session.delete(host + "studies/{}".format(i))

    # Delete investigations
    for i in range(0, 20):
        session.delete(host + "investigations/{}".format(i))

if __name__ == "__main__":
    main()