"""
This script automates the process of uploading data to the SEEK FAIRDOM platform. It reads metadata and measurement data from specified files, processes the data, and uploads it to the SEEK platform, creating the necessary structure of investigations, studies, and assays. The script is designed to work with PlantEye data from NPEC and assumes a specific data structure and naming convention.

The script requires the following command-line arguments:
1. Path to the datamatrix file.
2. Path to the investigation directory.
3. Paths to the CSV files containing measurement data.

The script uses the requests library to interact with the SEEK API and pandas for data manipulation.
"""

import sys
import os
import pandas
import pprint
import re
from collections import defaultdict
import requests
import json
import string

datamatrix_file = sys.argv[1]
investigationPath = sys.argv[2]
csvs = sys.argv[3:]

base_url = 'http://localhost:3000'

headers = {"Content-type": "application/vnd.api+json",
           "Accept": "application/vnd.api+json",
           "Accept-Charset": "ISO-8859-1"}

session = requests.Session()
session.headers.update(headers)
session.auth = ("capsicum.upload@wur.nl", "3#7B&GNC</yp2{k(")

containing_project_id = 2

columnsToDrop = ["ndvi_aver","ndvi_bin0","ndvi_bin1","ndvi_bin2","ndvi_bin3","ndvi_bin4","ndvi_bin5",
                 "greenness_aver","greenness_bin0","greenness_bin1","greenness_bin2","greenness_bin3","greenness_bin4","greenness_bin5",
                 "hue_aver","hue_bin0","hue_bin1","hue_bin2","hue_bin3","hue_bin4","hue_bin5",
                 "npci_aver","npci_bin0","npci_bin1","npci_bin2","npci_bin3","npci_bin4","npci_bin5",
                 "psri_aver","psri_bin0","psri_bin1","psri_bin2","psri_bin3","psri_bin4","psri_bin5"]

metadata = pandas.read_csv(datamatrix_file, sep=";")

def removeAfterSpaceFromDataMatrix(row):
    """
    Removes any text after a space in the 'DataMatrix' column of a row.

    Args:
        row (pandas.Series): A row from a DataFrame.

    Returns:
        pandas.Series: The modified row with updated 'DataMatrix' value.
    """
    row["DataMatrix"] = row["DataMatrix"].strip().split(" ")[0]
    return row

metadata = metadata.apply(removeAfterSpaceFromDataMatrix , axis=1)
datamatrix = os.path.basename(datamatrix_file).split(".")[0].split("_")

investigation = {}
investigation['data'] = {}
investigation['data']['type'] = 'investigations'
investigation['data']['attributes'] = {}
investigation['data']['attributes']['title'] = "_".join([datamatrix[4], datamatrix[3], datamatrix[2]])
investigation['data']['attributes']['description'] = "PlantEye data from NPEC"
investigation['data']['attributes']["creators"] = {}
investigation['data']['attributes']["creators"]["given_name"] = "Capsicum"
investigation['data']['attributes']["creators"]["family_name"] = "Upload"
investigation['data']['relationships'] = {}
investigation['data']['relationships']['projects'] = {}
investigation['data']['relationships']['projects']['data'] = [{'id' : containing_project_id, 'type' : 'projects'}]

print(investigation)
r = session.post(base_url + '/investigations', json=investigation)
investigation_id = r.json()['data']['id']
r.raise_for_status()

study = {}
study['data'] = {}
study['data']['type'] = 'studies'
study['data']['attributes'] = {}
study['data']['attributes']['title'] = datamatrix[3]
study['data']['attributes']["creators"] = {}
study['data']['attributes']['description'] = "PlantEye data from NPEC"
study['data']['attributes']["creators"]["given_name"] = "Capsicum"
study['data']['attributes']["creators"]["family_name"] = "Upload"
study['data']['relationships'] = {}
study['data']['relationships']['investigation'] = {}
study['data']['relationships']['investigation']['data'] = {'id' : investigation_id, 'type' : 'investigations'}
r = session.post(base_url + '/studies', json=study)
study_id = r.json()['data']['id']

os.makedirs("/".join([investigationPath, investigation['data']['attributes']['title']]), exist_ok=True)
metadata_csv = "/".join([investigationPath, investigation['data']['attributes']['title'], os.path.basename(datamatrix_file)])
metadata.to_csv(metadata_csv, sep="\t")

data_file = {}
data_file['data'] = {}
data_file['data']['type'] = 'data_files'
data_file['data']['attributes'] = {}
data_file['data']['attributes']['title'] = "Investigation metadata file for " + investigation['data']['attributes']['title']
data_file['data']['attributes']['description'] = "metadata file PlantEye data from NPEC"
data_file['data']['attributes']["creators"] = {}
data_file['data']['attributes']["creators"]["given_name"] = "Capsicum"
data_file['data']['attributes']["creators"]["family_name"] = "Upload"

local_blob = {'original_filename' : os.path.basename(metadata_csv), 'content_type' : "text/csv"}
data_file['data']['attributes']['content_blobs'] = [local_blob]
data_file['data']['relationships'] = {}
data_file['data']['relationships']['projects'] = {}
data_file['data']['relationships']['projects']['data'] = [{'id' : containing_project_id, 'type' : 'projects'}]

r = session.post(base_url + '/data_files', json=data_file)
r.raise_for_status()

populated_data_file = r.json()

data_file_id = populated_data_file['data']['id']
data_file_url = populated_data_file['data']['links']['self']

blob_url = populated_data_file['data']['attributes']['content_blobs'][0]['link']

upload = session.put(blob_url, data=open(metadata_csv,"r").read(), headers={'Content-Type': 'application/octet-stream'})
upload.raise_for_status()

checkAssayName = re.compile(r"f[0-9]+")
measurements = pandas.DataFrame()

def copyPots(row, pots):
    """
    Copies pot information from a pots DataFrame to a row based on matching coordinates.

    Args:
        row (pandas.Series): A row from a DataFrame.
        pots (pandas.DataFrame): A DataFrame containing pot information.

    Returns:
        pandas.Series: The modified row with updated pot information.
    """
    row["Pot"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Pot"].iloc[0]
    if "Treatment" in pots.columns:
        row["Treatment"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Treatment"].iloc[0]
    if "Experimemt" in pots.columns:
        row["Experiment"] = pots[ (pots["x"] == row["x"]) & (pots["y"] == row["y"]) ]["Experiment"].iloc[0]
    return row

def measurementsToFile(investigation, path, filename, measurements):
    """
    Saves the measurements DataFrame to a CSV file.

    Args:
        investigation (dict): The investigation dictionary.
        path (str): The directory path where the file will be saved.
        filename (str): The name of the file.
        measurements (pandas.DataFrame): The DataFrame containing measurements.

    Side Effects:
        Creates directories and writes a CSV file to the specified path.
    """
    os.makedirs(path + "/derived", exist_ok=True)
    measurements.to_csv(path + "/" + filename, sep=";")

def rawMeasurementsToFile(investigation, path, filename, measurements):
    """
    Saves the raw measurements to a CSV file.

    Args:
        investigation (dict): The investigation dictionary.
        path (str): The directory path where the file will be saved.
        filename (str): The name of the file.
        measurements (pandas.DataFrame): The DataFrame containing raw measurements.

    Returns:
        str: The full path to the saved file.

    Side Effects:
        Creates directories and writes a CSV file to the specified path.
    """
    os.makedirs(path + "/derived", exist_ok=True)
    df = pandas.DataFrame(measurements)
    df = df.transpose()
    df.to_csv(path + "/" + filename, sep="\t")
    return(path + "/" + filename)

def addPointClouds(row, title):
    """
    Adds a point cloud filename to a row based on its coordinates and timestamp.

    Args:
        row (pandas.Series): A row from a DataFrame.
        title (str): The title used in the filename.

    Returns:
        pandas.Series: The modified row with the point cloud filename added.
    """
    filename = "pointcloud/{}_{}_full_sx{:03d}_sy{:03d}.ply.gz".format(
        title, row["timestamp_file"], 
        row["x"],
        row["y"])
    row["pointcloud"] = filename
    return row

def createAssay(row, investigation, path, study_id):
    """
    Creates an assay and uploads the associated data file to the SEEK platform.

    Args:
        row (pandas.Series): A row from a DataFrame containing assay data.
        investigation (dict): The investigation dictionary.
        path (str): The directory path for saving files.
        study_id (str): The ID of the study to which the assay belongs.

    Side Effects:
        Creates directories, writes files, and uploads data to the SEEK platform.
    """
    data_file = {}

    filename = "derived/" + assay.title + ".csv"
    fullPath = "/".join([investigation.title, investigation.studies[0].title, sample.name, assay.title, description["technologyType"], description["technologyPlatform"]])
    fullFilename = rawMeasurementsToFile(investigation, path + "/" + fullPath , filename, row)

    data_file['data'] = {}
    data_file['data']['type'] = 'data_files'
    data_file['data']['attributes'] = {}
    data_file['data']['attributes']['title'] = "Investigation metadata file for " + investigation['data']['attributes']['title']
    data_file['data']['attributes']['description'] = "PlantEye measurement file from NPEC"
    data_file['data']['attributes']["creators"] = {}
    data_file['data']['attributes']["creators"]["given_name"] = "Capsicum"
    data_file['data']['attributes']["creators"]["family_name"] = "Upload"
    
    local_blob = {'original_filename' : os.path.basename(fullFilename), 'content_type' : "text/csv"}
    data_file['data']['attributes']['content_blobs'] = [local_blob]
    data_file['data']['relationships'] = {}
    data_file['data']['relationships']['projects'] = {}
    data_file['data']['relationships']['projects']['data'] = [{'id' : containing_project_id, 'type' : 'projects'}]
    
    r = session.post(base_url + '/data_files', json=data_file)
    r.raise_for_status()
    
    populated_data_file = r.json()

    data_file_id = populated_data_file['data']['id']
    data_file_url = populated_data_file['data']['links']['self']
    
    blob_url = populated_data_file['data']['attributes']['content_blobs'][0]['link']
    
    upload = session.put(blob_url, data=open(fullFilename,"r").read(), headers={'Content-Type': 'application/octet-stream'})
    upload.raise_for_status()
    
    assay = {}
    assay['data'] = {}
    assay['data']['type'] = 'assays'
    assay['data']['attributes'] = {}
    assay['data']['attributes']['title'] = row["timestamp_file"]
    assay['data']['attributes']['assay_class'] = {'key' : 'EXP'}
    assay['data']['attributes']['assay_type'] = {'uri' : 'http://jermontology.org/ontology/JERMOntology#phenotype'}
    assay['data']['attributes']['technology_type'] = {'uri' : 'http://jermontology.org/ontology/JERMOntology#Imaging'}
    assay['data']['relationships'] = {}
    assay['data']['relationships']['study'] = {}
    assay['data']['relationships']['study']['data'] = {'id' : study_id, 'type' : 'studies'}
    assay['data']['relationships']['organism'] = {}
    assay['data']['relationships']['organism']['data'] = {'id' : 1, 'type' : 'organisms'}

def finalize(investigation, measurements, investigationPath, title, metadata, study_id):
    """
    Finalizes the processing of measurements by creating assays and saving data files.

    Args:
        investigation (dict): The investigation dictionary.
        measurements (pandas.DataFrame): The DataFrame containing measurements.
        investigationPath (str): The directory path for saving files.
        title (str): The title used in filenames.
        metadata (pandas.DataFrame): The DataFrame containing metadata.
        study_id (str): The ID of the study to which the assays belong.

    Side Effects:
        Creates directories, writes files, and uploads data to the SEEK platform.
    """
    if "Pot" in measurements.columns:
        pots = measurements.dropna(axis=0, subset=["Pot"])
        if len(pots) > 0 and "Pot" in pots.columns:
            pots = pots.drop_duplicates(subset=["x","y"], keep="first")
            if len(pots) and pots.dtypes["Pot"] == metadata.dtypes["DataMatrix"] and len(metadata.merge(pots, how="inner", left_on="DataMatrix", right_on="Pot")) > 0:
                measurements = measurements.drop(columnsToDrop, axis=1)
                measurements = measurements.apply(copyPots , axis=1, pots=pots)
                measurements = measurements.apply(addPointClouds, axis=1, title=title)
                measurements.apply(createAssay, axis=1, investigation = investigation, path = investigationPath, study_id = study_id)
                investigation.measurements = pandas.concat([investigation.measurements, measurements], axis=0, ignore_index=True)

previousAssay = ""
for csv in csvs:
    assayName = os.path.basename(csv).split("_")[0]
    timestamp = os.path.basename(csv).split("_")[1]
    if checkAssayName.match(assayName) != None:
        try: 
            currentMeasurements = pandas.read_csv(csv, sep="\t", skiprows=[1])
            currentMeasurements["timestamp_file"] = timestamp
            
            if previousAssay == assayName:
                if len(measurements) == 0:
                    measurements = currentMeasurements
                else:
                    measurements = pandas.concat([measurements, currentMeasurements], axis=0, ignore_index=True)
            else:
                if len(measurements) > 0:
                    finalize(investigation, measurements, investigationPath, previousAssay, metadata, study_id)
                
                measurements = currentMeasurements
            previousAssay = assayName
        except:
            pass
    else:
        pass

if len(measurements) > 0:
    finalize(investigation, measurements, investigationPath, previousAssay, metadata, study_id)

measurementsToFile(investigation, "/".join([investigationPath, investigation.title, investigation.studies[0].title]), "derived/" + investigation.studies[0].title + ".csv", investigation.measurements)