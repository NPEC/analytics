import sys
import os
import json
from isatools.isajson import ISAJSONEncoder
import isatools
from isatools.model import *
from isatools import isatab
from isatools import isajson
from collections import defaultdict
import shutil
import gzip
import open3d as o3d
import tempfile
import numpy as np

"""
This script processes ISA-JSON files and associated point cloud data files.
It extracts greenness information from point clouds and writes histograms
of the greenness values to specified output files. The script requires an
ISA-JSON file and a list of point cloud files as input.
"""

investigation = isajson.load(open(sys.argv[1], "r"))
study = investigation.studies[0]    
BINS = 256

def writeHistogram(data, filename):
    """
    Writes a histogram of the given data to a file.

    Parameters:
        data (numpy.ndarray): The data for which the histogram is to be computed.
        filename (str): The name of the file where the histogram will be written.

    Outputs:
        A file containing the histogram data. The bin edges and histogram counts
        are written in separate lines, separated by semicolons.

    Side Effects:
        Creates or overwrites the specified file with histogram data.
    """
    hist, bin_edges = np.histogram(data, bins=BINS)
    with open(filename, "w") as f:
        f.write(";".join(map(str, bin_edges)))
        f.write("\n")
        f.write(";".join(map(str, hist)))

def get_greenness(pcd):
    """
    Calculates the greenness index for a point cloud.

    Parameters:
        pcd (open3d.geometry.PointCloud): The point cloud object containing wavelength data.

    Returns:
        numpy.ndarray: An array of greenness values for each point in the point cloud.

    Exceptions:
        May raise an exception if the point cloud does not contain wavelength data.

    Notes:
        The greenness index is calculated using the formula:
        (R - B + 2G) / (R + G + B), where R, G, and B are the red, green, and blue
        wavelength values, respectively.
    """
    np.seterr(divide='ignore', invalid='ignore')
    return ((np.asarray(pcd.wavelengths)[:,0] - np.asarray(pcd.wavelengths)[:,2] + 
             2.0 * np.asarray(pcd.wavelengths)[:,1]) / 
            (np.asarray(pcd.wavelengths)[:,0] + np.asarray(pcd.wavelengths)[:,1] + 
             np.asarray(pcd.wavelengths)[:,2]))

# Find each point cloud in the file list
pointclouds = defaultdict(str)
for pcd in sys.argv[2:]:
    filename = os.path.basename(pcd)
    pointclouds[filename] = pcd

# Process ISA file
for a in study.assays:
    print(a.data_files)
    for df in a.data_files:
        for com in df.comments:
            if com.name == "fullPath":
                print(com.value)
                print(os.path.basename(df.filename))
                if ".ply" in df.filename and os.path.basename(df.filename) in pointclouds:
                    # Copy ply
                    shutil.copy2(pointclouds[os.path.basename(df.filename)], com.value)
                    a.pointcloud = com.value

for a in study.assays:
    print(a.data_files)
    if a.pointcloud:
        pcd = gzip.GzipFile(a.pointcloud, "rb")
        tempFile = tempfile.TemporaryFile()
        tempFile.write(pcd.read())
        tempFile.seek(0)
        pcd = o3d.io.read_point_cloud(tempFile.name)
        for df in a.data_files:
            for com in df.comments:
                if com.name == "fullPath":
                    if "greenness.csv" in com.value:
                        greenness = get_greenness(pcd)
                        writeHistogram(greenness, com.value)