"""
This script serves as a command-line interface for the F500 class, which provides various functionalities such as restructuring data, processing point clouds, verifying data, combining histograms, and uploading data. The script determines the command to execute based on user input.

Modules:
    - sys: Provides access to some variables used or maintained by the interpreter and to functions that interact with the interpreter.
    - os: Provides a portable way of using operating system-dependent functionality.
    - pandas: A data manipulation and analysis library for Python.
    - F500: A custom module that contains the F500 class with methods for different data processing tasks.

Usage:
    Run the script with the desired command to execute the corresponding functionality.
"""

import sys
import os
import pandas

from F500 import F500

def main():
    """Main function to execute the command-line interface for the F500 class.

    This function initializes an instance of the F500 class and executes a method
    based on the command-line arguments provided by the user.

    Side Effects:
        Executes a method of the F500 class based on user input.

    Raises:
        Exception: If an invalid command is provided.
    """
    f500 = F500()
    f500.commandLineInterface()
    if f500.args.command == "restructure":
        f500.restructure()
    elif f500.args.command == "pointclouds":
        f500.processPointclouds()
    elif f500.args.command == "verify":
        f500.verify()
    elif f500.args.command == "histogram":
        f500.combineHistograms()
    elif f500.args.command == "upload":
        f500.upload()
    else:
        raise Exception("Invalid command provided.")

if __name__ == '__main__':
    main()
