---
title: "F500 data overview"
author: "Sven Warris"
date: "7/21/2021"
output:
  html_document:
    df_print: paged
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(ggplot2)
library(readr)
library(dplyr)
library(data.table)
library(kableExtra)
library(tidyverse)
library(plot.matrix)
```

# Comments on the data files

Please note the following observations on the data files. They might interfere with downstream analyses.

- The second row (first after the header) contains the data types, no data
- There appears to be an empty column _with no header_ in the file. 
- Not all files contain the Experiment, Treatment and/or Pot columns 
- After reading all files, some observations are still not linked to a pot
- DataMatrix in metadata contains whitespaces, the Pot identifier in the data files stop at the first whitespace

TODO:

- keep track of the files with errors
- sapply gives many errors, looks like these files contain one 1 measurement?

# Setup

```{r echo=FALSE}
dataPath = "./data/SUCCED"
metadataPath = "./metadata"
gantry = "B"
fullPath = paste(dataPath,gantry, sep="/")
experiments = c("pepper", "Petunia", "Chrysant", "Quinoa")
startDate = parse_datetime("20210601", format = "%Y%m%d")

```

Reading CSV files from `r dataPath` for gantry `r gantry`. 
Start date: `r startDate`
Description of the output columns: [planteye parameters](https://phenospex.helpdocs.com/planteye/planteye-parameters)

```{r echo=FALSE, warning=FALSE, error=FALSE, message=FALSE}
# Get file listing
fileList = list.files(path=fullPath, pattern = ".*csv",full.names = TRUE)

# create empty objects to start with
fullStats = NULL
stats = NULL
currentSample = NULL
potNames = NULL

#process each file
for (f in fileList) {
  sample = strsplit(basename(f), "_", fixed = TRUE)[[1]][1]
  # Assume first part of the file name indicates sample name. 
  # Use this to correct for missing pots
  if (is.null(currentSample) || sample != currentSample) {
    if (!is.null(potNames)) {
      # join on (x,y) coordinates in plot
      stats = stats %>% left_join(potNames, by = c("x","y"))
      try({fullStats = rbind(fullStats, stats)})
    }
    stats = NULL
    potNames = NULL
    currentSample = sample
  }
  # read file
  dataset = read_table2(f)
  # skip data type line
  dataset = dataset[-1,]

  # a file with missing pot values has an empty column X48
  if ("X48" %in% colnames(dataset)) {
    dataset = dataset[,!(names(dataset) == "X48")]
  }
  else {
    # some files are missing the treatment values
    if ("Treatment" %in% colnames(dataset)) {
      currentPotNames = dataset[!is.na(dataset$Pot), c("x","y", "Pot", "Treatment", "Experiment")]
      l = length(colnames(dataset))
      dataset = dataset[,-c(l-2, l-1, l)] 
    }
    else {
      currentPotNames = dataset[!is.na(dataset$Pot), c("x","y", "Pot", "Experiment")]
      currentPotNames$Treatment = NA
      l = length(colnames(dataset))
      dataset = dataset[,-c(l-1, l)] 
    }
    # keep track of pot names to add to the files with missing pot names
    if (is.null(potNames) || length(currentPotNames$Pot) > length(potNames$Pot)) {
      potNames = currentPotNames
    }
  }
  try( {
    stats = rbind(stats, dataset)
    })
}
if (!is.null(potNames)) {
  stats = stats %>% left_join(potNames, by = c("x","y"))
  try({fullStats = rbind(fullStats, stats)})
}

# remove any rows with no pots associated with the data
fullStats = fullStats %>% drop_na(Pot)
# make columns numeric
fullStats[,2:(length(fullStats)-3)] = sapply(fullStats[,2:(length(fullStats)-3)], as.numeric)
# add an additional time column for better reading
#fullStats$time = as.character(parse_datetime(fullStats$timestamp, format="%Y%m%dT%H%M%S"), format = "%Y-%m-%d %H:%M")
fullStats$time = parse_datetime(fullStats$timestamp, format="%Y%m%dT%H%M%S")
# remove all data before start date:
fullStats = fullStats %>% filter(time >= startDate)

```

The resulting data set contains `r length(rownames(fullStats))` observations for `r length(unique(fullStats$Pot))` pots.

## Metadata

```{r echo=FALSE, warning=FALSE, error=FALSE, message=FALSE}
# Get file listing
fileList = list.files(path=metadataPath, pattern = ".*csv",full.names = TRUE)

metadata = NULL
#process each file
for (f in fileList) {
  dataset = read_csv2(f)
  metadata = rbind(metadata, dataset)
}
metadata$Plot = substr(metadata$Position,1,2)
metadata = metadata %>% separate(DataMatrix, c("Pot"), " ", extra = "drop", fill ="right")
fullStats = fullStats %>% left_join(metadata, by=c("Pot"))
```

The metadata files contain `r length(rownames(metadata))` entries with `r length(unique(metadata$Genotype))` unique genotypes and `r length(unique(metadata$SoilType))` unique soil types.

## Overview of all pots

```{r echo=FALSE}
ggplot(fullStats, mapping = aes(x=time, y=height, color=Pot)) + geom_line(show.legend = FALSE, mapping=aes(group=Pot)) + ggtitle("Plant height, colored by pot") 

ggplot(fullStats, mapping = aes(x=time, y=leaf_area_index, color=Pot)) + geom_line(show.legend = FALSE) + ggtitle("Leaf area index, colored by pot") 
ggplot(fullStats, mapping = aes(x=time, y=digital_biomass, color=Pot)) + geom_line(show.legend = FALSE) + ggtitle("Biomass, colored by pot") 

ggplot(fullStats, mapping = aes(x=height, y=leaf_area_index, color=Pot)) + geom_point(show.legend = FALSE) + ggtitle("Plant height vs. leaf area index, colored by pot") 
ggplot(fullStats, mapping = aes(x=height, y=digital_biomass, color=Pot)) + geom_point(show.legend = FALSE) + ggtitle("Plant height vs. biomass, colored by pot") 

```

## Overview of all plots

```{r echo=FALSE}
ggplot(fullStats, mapping = aes(x=time, y=height, color=Plot)) + geom_line(show.legend = TRUE, mapping=aes(group=Pot)) + ggtitle("Plant height, colored by plot") 

ggplot(fullStats, mapping = aes(x=time, y=leaf_area_index, color=Plot)) + geom_line(show.legend = TRUE, mapping=aes(group=Pot)) + ggtitle("Leaf area index, colored by plot") 
ggplot(fullStats, mapping = aes(x=time, y=digital_biomass, color=Plot)) + geom_line(show.legend = TRUE, mapping=aes(group=Pot)) + ggtitle("Biomass, colored by plot") 

ggplot(fullStats, mapping = aes(x=height, y=leaf_area_index, color=Plot)) + geom_point(show.legend = TRUE) + ggtitle("Plant height vs. leaf area index, colored by plot") 
ggplot(fullStats, mapping = aes(x=height, y=digital_biomass, color=Plot)) + geom_point(show.legend = TRUE) + ggtitle("Plant height vs. biomass, colored by plot") 

```

# Project data

```{r echo=FALSE}

for (experiment in experiments) {
  experimentData = fullStats[grepl(experiment,fullStats$Pot,fixed=TRUE),]
  if (nrow(experimentData) > 0 ) {
    print(ggplot(experimentData, mapping = aes(x=time, y=height, color=Pot)) + geom_line(show.legend = FALSE) + ggtitle(paste(experiment, "plant height, colored by pot")) )
    
    print(ggplot(experimentData, mapping = aes(x=time, y=leaf_area_index, color=Pot)) + geom_line(show.legend = FALSE) + ggtitle(paste(experiment, "leaf area index, colored by pot"))) 

    print(ggplot(experimentData, mapping = aes(x=time, y=digital_biomass, color=Pot)) + geom_line(show.legend = FALSE) + ggtitle(paste(experiment, "biomass, colored by pot"))) 
    
    #print(ggplot(experimentData, mapping = aes(x=height, y=leaf_area_index, color=Pot)) + geom_point(show.legend = FALSE) + ggtitle(paste(experiment, "plant height vs. leaf area index, colored by pot")) )

    print(ggplot(experimentData, mapping = aes(x=height, y=digital_biomass, color=Pot)) + geom_point(show.legend = FALSE) + ggtitle(paste(experiment, "plant height vs. biomass, colored by pot")) )
    
    # print(ggplot(experimentData, mapping = aes(x=time, y=height, color=Treatment)) + geom_line(mapping = aes(group=Pot)) + ggtitle(paste(experiment, "plant height, colored by treatment")) )
    # print(ggplot(experimentData, mapping = aes(x=time, y=leaf_area_index, color=Treatment)) + geom_line(mapping = aes(group=Pot)) + ggtitle(paste(experiment, "leaf area index, colored by treatment")) )
    # print(ggplot(experimentData, mapping = aes(x=time, y=digital_biomass, color=Treatment)) + geom_line(mapping = aes(group=Pot)) + ggtitle(paste(experiment, "biomass, colored by treatment")) )
    # 
    # print(ggplot(experimentData, mapping = aes(x=height, y=leaf_area_index, color=Treatment)) + geom_point() + ggtitle(paste(experiment, "plant height vs. leaf area index, colored by treatment")) )
    # print(ggplot(experimentData, mapping = aes(x=height, y=digital_biomass, color=Treatment)) + geom_point() + ggtitle(paste(experiment, "plant height vs. biomass, colored by treatment")) )
    showLegend = length(unique(experimentData$Genotype)) <= 15
    print(ggplot(experimentData, mapping = aes(x=time, y=height, color=Genotype)) + geom_line(mapping = aes(group=Pot), show.legend = showLegend) + ggtitle(paste(experiment, "plant height, colored by genotype")) )
    print(ggplot(experimentData, mapping = aes(x=time, y=leaf_area_index, color=Genotype)) + geom_line(mapping = aes(group=Pot), show.legend = showLegend) + ggtitle(paste(experiment, "leaf area index, colored by genotype")) )
    print(ggplot(experimentData, mapping = aes(x=time, y=digital_biomass, color=Genotype)) + geom_line(mapping = aes(group=Pot), show.legend = showLegend) + ggtitle(paste(experiment, "biomass, colored by genotype")) )

    print(ggplot(experimentData, mapping = aes(x=height, y=leaf_area_index, color=Genotype)) + geom_point(show.legend = showLegend) + ggtitle(paste(experiment, "plant height vs. leaf area index, colored by genotype")) )
    print(ggplot(experimentData, mapping = aes(x=height, y=digital_biomass, color=Genotype)) + geom_point(show.legend = showLegend) + ggtitle(paste(experiment, "plant height vs. biomass, colored by genotype")) ) 
  }
}
```

## Top view of the gantry

```{r echo=FALSE, eval=TRUE, out.width="100%"}

for (experiment in experiments) {
  experimentData = fullStats[grepl(experiment,fullStats$Pot,fixed=TRUE),]
  if (nrow(experimentData) > 0 ) {
    par(mar=c(5.1, 4.1, 4.1, 4.1))
    # number of plots:
    plots = unique(experimentData$Plot)
    plots = plots[order(plots)]
    # max dimension of matrix
    maxX = max(experimentData$x)
    maxY = max(experimentData$y)
    tableView = NULL 
    plotNumbers = 1:length(plots)
    for (p in plotNumbers) {
      for (x in 0:maxX) {
        for (y in 0:maxY) {
          genotype = unique(experimentData[which(experimentData$x == x & experimentData$y == y & experimentData$Plot == plots[p]),]$Genotype)
          maxHeight = experimentData[which(experimentData$x == x & experimentData$y == y & experimentData$Plot == plots[p]),]$height_max
          if (length(genotype) <= 0 ) {
            genotype = NA
          }
          if (length(maxHeight) <= 0 ) {
            maxHeight = NA
          }
          else {
            maxHeight = max(maxHeight)
          }
          thisRow = data.frame(x=x+p-1, y=y, Genotype = genotype, height_max=maxHeight)
          tableView = rbind(tableView, thisRow)
        }
      }
    }
    tableViewMatrix = pivot_wider(tableView %>% select(-height_max), names_from = y, values_from = Genotype)
    tableViewMatrix = t(as.matrix(tableViewMatrix %>% select(-x)))
    print("Genotype overview")
    try({plot(tableViewMatrix, asp=FALSE, xlab='', ylab='', main=paste(experiment, ", genotypes", sep=""))})
    print("Maximum height recorded")
    tableViewMatrix = pivot_wider(tableView %>% select(-Genotype), names_from = y, values_from = height_max)
    tableViewMatrix = t(as.matrix(tableViewMatrix %>% select(-x)))
    try({plot(tableViewMatrix, asp=FALSE, xlab='', ylab='', main=paste(experiment, ", maximum height", sep=""))})
  }
}

```
