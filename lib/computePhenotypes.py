"""
This script provides functions to calculate various vegetation indices from point cloud data (PCD) using specific wavelength channels. 
These indices include the Normalized Difference Vegetation Index (NDVI) for visualization, NDVI, the Normalized Pigment Chlorophyll Index (NPCI), 
and a greenness index. The calculations are based on the wavelengths corresponding to different spectral bands.

Functions:
- get_ndvi_for_visualization: Computes NDVI for visualization purposes, scaling the result between 0 and 1.
- get_ndvi: Computes the standard NDVI, with values ranging from -1 to 1.
- get_npci: Computes the NPCI using the red and blue channels.
- get_greenness: Computes a greenness index using the red, green, and blue channels.
"""

import numpy as np

def get_ndvi_for_visualization(pcd):
    """
    Calculate the NDVI for visualization purposes, scaling the result between 0 and 1.

    Parameters:
    pcd (object): A point cloud data object containing 'wavelengths' and 'ndvi' attributes. 
                  'wavelengths' is expected to be a 2D array where the columns correspond to different spectral bands.

    Returns:
    ndarray: A 1D array of NDVI values scaled between 0 and 1.

    Side Effects:
    - Modifies the 'ndvi' attribute of the input 'pcd' object.

    Notes:
    - This function ignores division and invalid operation warnings using numpy's seterr function.
    """
    np.seterr(divide='ignore', invalid='ignore')
    np.asarray(pcd.ndvi)[:, 0] = (((np.asarray(pcd.wavelengths)[:, 3] - np.asarray(pcd.wavelengths)[:, 0]) /
                                   (np.asarray(pcd.wavelengths)[:, 3] + np.asarray(pcd.wavelengths)[:, 0])) + 1) / 2
    return pcd.ndvi[:, 0]

def get_ndvi(pcd):
    """
    Calculate the standard NDVI, with values ranging from -1 to 1.

    Parameters:
    pcd (object): A point cloud data object containing 'wavelengths' and 'ndvi' attributes. 
                  'wavelengths' is expected to be a 2D array where the columns correspond to different spectral bands.

    Returns:
    ndarray: A 1D array of NDVI values ranging from -1 to 1.

    Side Effects:
    - Modifies the 'ndvi' attribute of the input 'pcd' object.

    Notes:
    - This function ignores division and invalid operation warnings using numpy's seterr function.
    """
    np.seterr(divide='ignore', invalid='ignore')
    np.asarray(pcd.ndvi)[:, 0] = (np.asarray(pcd.wavelengths)[:, 3] - np.asarray(pcd.wavelengths)[:, 0]) / \
                                 (np.asarray(pcd.wavelengths)[:, 3] + np.asarray(pcd.wavelengths)[:, 0])
    return pcd.ndvi[:, 0]

def get_npci(pcd):
    """
    Calculate the Normalized Pigment Chlorophyll Index (NPCI) using the red and blue channels.

    Parameters:
    pcd (object): A point cloud data object containing 'wavelengths' attribute. 
                  'wavelengths' is expected to be a 2D array where the columns correspond to different spectral bands.

    Returns:
    ndarray: A 1D array of NPCI values.

    Notes:
    - This function ignores division and invalid operation warnings using numpy's seterr function.
    """
    np.seterr(divide='ignore', invalid='ignore')
    return ((np.asarray(pcd.wavelengths)[:, 0] - np.asarray(pcd.wavelengths)[:, 2]) /
            (np.asarray(pcd.wavelengths)[:, 0] + np.asarray(pcd.wavelengths)[:, 2]))

def get_greenness(pcd):
    """
    Calculate a greenness index using the red, green, and blue channels.

    Parameters:
    pcd (object): A point cloud data object containing 'wavelengths' attribute. 
                  'wavelengths' is expected to be a 2D array where the columns correspond to different spectral bands.

    Returns:
    ndarray: A 1D array of greenness index values.

    Notes:
    - This function ignores division and invalid operation warnings using numpy's seterr function.
    """
    np.seterr(divide='ignore', invalid='ignore')
    return ((np.asarray(pcd.wavelengths)[:, 0] - np.asarray(pcd.wavelengths)[:, 2] + 
             2.0 * np.asarray(pcd.wavelengths)[:, 1]) /
            (np.asarray(pcd.wavelengths)[:, 0] + np.asarray(pcd.wavelengths)[:, 1] + np.asarray(pcd.wavelengths)[:, 2]))