import numpy as np

"""
This script provides functionality to rescale the wavelengths of a point cloud data (PCD) object. 
The script modifies the color and NIR (Near-Infrared) attributes of the PCD based on the provided scale.
"""

def rescale_wavelengths(pcd, scale):
    """
    Rescales the wavelengths of a point cloud data (PCD) object by a given scale factor.

    This function takes a PCD object with attributes for wavelengths, colors, and NIR values. 
    It rescales the wavelengths by dividing them by the provided scale factor and updates the 
    color and NIR attributes of the PCD accordingly.

    Parameters:
    pcd : object
        A point cloud data object that contains 'wavelengths', 'colors', and 'nir' attributes.
        The 'wavelengths' attribute is expected to be a 2D array with columns representing 
        R, G, B, and NIR wavelengths.
    scale : float
        The scale factor by which to divide the wavelengths.

    Returns:
    object
        The modified PCD object with rescaled color and NIR attributes.

    Side Effects:
    Modifies the 'colors' and 'nir' attributes of the input PCD object in place.

    Exceptions:
    This function assumes that the input PCD object has the required attributes and that they 
    are in the expected format. If not, it may raise AttributeError or IndexError.

    Future Work:
    Consider adding input validation to ensure the PCD object has the required attributes and 
    that they are in the expected format. Additionally, handle potential exceptions more gracefully.
    """
    tmp_wvl = np.asarray(pcd.wavelengths) / scale
    np.asarray(pcd.colors)[:, 0] = tmp_wvl[:, 0]
    np.asarray(pcd.colors)[:, 1] = tmp_wvl[:, 1]
    np.asarray(pcd.colors)[:, 2] = tmp_wvl[:, 2]
    np.asarray(pcd.nir)[:, 0] = tmp_wvl[:, 3]
    return pcd