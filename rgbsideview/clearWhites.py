"""
This script reads a PLY file containing a 3D point cloud and loads it into an Open3D PointCloud object.

The script uses the Open3D library to handle the point cloud data. It reads a PLY file specified as a command-line argument and loads the data into a PointCloud object for further processing or visualization.

Usage:
    python script_name.py <path_to_ply_file>

Dependencies:
    - Open3D: Install via `pip install open3d`
    - NumPy: Install via `pip install numpy`

Note:
    Ensure that the Open3D library is correctly installed and that the PLY file path is valid.
"""

import open3d as o3d
import numpy
import sys

def main():
    """
    Main function to read a PLY file and load it into an Open3D PointCloud object.

    This function reads a PLY file specified as a command-line argument and loads it into a PointCloud object using the Open3D library.

    Raises:
        IndexError: If no command-line argument is provided for the PLY file path.
        FileNotFoundError: If the specified PLY file does not exist.
        Exception: If there is an error reading the PLY file.

    Side Effects:
        Loads the point cloud data into memory.

    Future Work:
        - Add error handling for unsupported file formats.
        - Implement visualization of the point cloud data.
    """
    try:
        ply = o3d.io.read_point_cloud(sys.argv[1], format="ply")
        print("Point cloud successfully loaded.")
    except IndexError:
        print("Error: No PLY file path provided. Please specify the path to a PLY file as a command-line argument.")
    except FileNotFoundError:
        print(f"Error: The file '{sys.argv[1]}' does not exist. Please provide a valid file path.")
    except Exception as e:
        print(f"An error occurred while reading the PLY file: {e}")

if __name__ == "__main__":
    main()