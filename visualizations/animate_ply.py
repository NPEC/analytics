import open3d as o3d
import numpy as np
import sys
import time

"""
This script reads a list of Phenospex PLY PointClouds and creates an animation.
In the /tmp directory, a set of PNG images are created, which can be stitched into an animated GIF.
For example: convert -delay 20 -loop 0 *.png animation.gif
Requires the NPEC Open3D additions (https://github.com/swarris/Open3D/tree/NPEC).
In the first for-loop, you can set colors to:
pcd.colors
pcd.nir
pcd.ndvi
"""

pcds = []

for i in sys.argv[1:]:
    pcd = o3d.io.read_point_cloud(i)
    pcd.wavelengthsToData(1000)
    pcd.computeNDVI()
    pcd.colors = pcd.colors
    pcds.append(pcd)

def play_motion(list_of_pcds):
    """
    Plays an animation of a list of point clouds using Open3D's visualization tools.

    This function initializes a visualizer and iterates through the provided list of point clouds,
    updating the visualizer with each point cloud to create an animation effect. It captures each
    frame as a PNG image in the /tmp directory.

    Args:
        list_of_pcds (list): A list of Open3D point cloud objects to be animated.

    Side Effects:
        - Creates PNG images in the /tmp directory for each frame of the animation.
        - Opens a visualization window to display the animation.

    Notes:
        - The function uses a nested callback structure to handle forward and backward animation.
        - Future work could include adding more control over the animation speed and direction.
    """
    play_motion.vis = o3d.visualization.Visualizer()
    play_motion.index = 0

    def reset_motion(vis):
        """
        Resets the animation to the first frame.

        Args:
            vis: The Open3D visualizer instance.

        Returns:
            bool: Always returns False to indicate the animation should not stop.
        """
        play_motion.index = 0
        pcd.points = list_of_pcds[0].points
        pcd.colors = list_of_pcds[0].colors
        vis.update_geometry(pcd)
        vis.poll_events()
        vis.update_renderer()
        time.sleep(.25)
        vis.register_animation_callback(forward)
        return False

    def backward(vis):
        """
        Moves the animation one frame backward.

        Args:
            vis: The Open3D visualizer instance.
        """
        pm = play_motion

        if pm.index > 0:
            pm.index -= 1
            pcd.points = list_of_pcds[pm.index].points
            pcd.colors = list_of_pcds[pm.index].colors
            time.sleep(.05)
            vis.update_geometry(pcd)
            vis.poll_events()
            vis.update_renderer()
        else:
            vis.register_animation_callback(forward)

    def forward(vis):
        """
        Moves the animation one frame forward and captures the frame as a PNG image.

        Args:
            vis: The Open3D visualizer instance.

        Returns:
            bool: Always returns False to indicate the animation should not stop.
        """
        pm = play_motion
        if pm.index < len(list_of_pcds) - 1:
            pm.index += 1
            pcd.points = list_of_pcds[pm.index].points
            pcd.colors = list_of_pcds[pm.index].colors
            time.sleep(.05)
            vis.update_geometry(pcd)
            vis.poll_events()
            vis.update_renderer()
            vis.capture_screen_image("/tmp/animation_{}.png".format(pm.index))
        else:
            vis.register_animation_callback(backward)
        return False

    # Geometry of the initial frame
    pcd = list_of_pcds[0]

    # Initialize Visualizer and start animation callback
    vis = play_motion.vis
    vis.create_window()
    vis.add_geometry(pcd)
    vis.register_animation_callback(forward)
    vis.run()
    vis.destroy_window()

play_motion(pcds)