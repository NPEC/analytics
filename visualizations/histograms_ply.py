import open3d as o3d
import sys
import numpy
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

"""
This script reads a Phenospex PLY PointCloud file specified as a command-line argument
and displays histograms for RGB, NIR, NDVI, and intensity values. It also generates and
displays 3D images for RGB, NIR, and NDVI. The script requires the NPEC Open3d additions
(https://github.com/swarris/Open3D/tree/NPEC).
"""

r = []
g = []
b = []
ndvi = []
nir = []
intensity = []
bins = 100
scale = 500

def createPNG(pcd, filename):
    """
    Creates a PNG image from a point cloud.

    Parameters:
    pcd (open3d.geometry.PointCloud): The point cloud to visualize and capture.
    filename (str): The filename where the image will be saved.

    Side Effects:
    Opens a visualization window and saves a screenshot as a PNG file.
    """
    vis = o3d.visualization.Visualizer()
    vis.create_window(visible=True)
    vis.add_geometry(pcd)
    vis.update_geometry(pcd)
    vis.run()
    vis.capture_screen_image(filename)
    vis.destroy_window()

# Read the point cloud from the file specified in the command-line argument
pcd = o3d.io.read_point_cloud(sys.argv[1])

# Calculate colors and NIR based on wavelength data
pcd.wavelengthsToData(scale)

for c in pcd.colors:
    r.append(c[0] * scale)
    g.append(c[1] * scale)
    b.append(c[2] * scale)

fig, axs = plt.subplots(3, 3, sharey=False, tight_layout=True)
axs[0][0].hist(r, bins=bins, color="red")
axs[0][0].set_title("Red", color="red")
axs[0][1].hist(g, bins=bins, color="green")
axs[0][1].set_title("Green", color="green")
axs[1][0].hist(b, bins=bins, color="blue")
axs[1][0].set_title("Blue", color="blue")

for n in pcd.nir:
    nir.append(n[0] * scale)

axs[1][1].hist(nir, bins=bins, color="m")
axs[1][1].set_title("Near-IR", color="m")

# Compute NDVI based on wavelength data (R + NIR)
pcd.computeNDVI()
for n in pcd.ndvi:
    ndvi.append((n[0] - 0.5) * 2.0)

axs[2][0].hist(ndvi, bins=bins, color="olive")
axs[2][0].set_title("NDVI", color="olive")

for n in pcd.intensity:
    intensity.append(n[0] * 2**8)

axs[2][1].hist(intensity, bins=bins, color="grey")
axs[2][1].set_title("Intensity", color="grey")

createPNG(pcd, "/tmp/ply_rgb.png")
pcd.colors = pcd.nir
createPNG(pcd, "/tmp/ply_nir.png")
pcd.colorizeNDVI()
pcd.colors = pcd.ndvi
createPNG(pcd, "/tmp/ply_ndvi.png")

axs[0][2].imshow(mpimg.imread("/tmp/ply_rgb.png"))
axs[0][2].set_title("RGB image")
axs[1][2].imshow(mpimg.imread("/tmp/ply_nir.png"))
axs[1][2].set_title("NIR image")
axs[2][2].imshow(mpimg.imread("/tmp/ply_ndvi.png"))
axs[2][2].set_title("NDVI image")

plt.show()