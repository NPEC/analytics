import open3d as o3d
import sys
import numpy

"""
This script demonstrates the use of Open3D to process and visualize point cloud data from a Phenospex PLY PointCount file.
It includes functionalities to read point cloud data, visualize it, compute NDVI (Normalized Difference Vegetation Index),
and colorize the NDVI data for better visualization.

Usage:
    python script_name.py <path_to_ply_file>

Dependencies:
    - Open3D
    - NumPy

Note:
    This script assumes that the point cloud data contains wavelength information necessary for NDVI computation.
    The script also assumes the existence of certain methods like `wavelengthsToData`, `computeNDVI`, and `colorizeNDVI`
    which may not be part of the standard Open3D library.
"""

def main():
    """
    Main function to execute the point cloud processing and visualization.

    This function reads a point cloud file specified by the command line argument, visualizes it, computes NDVI,
    and visualizes the NDVI data. It also colorizes the NDVI data for enhanced visualization.

    Raises:
        IndexError: If no file path is provided as a command line argument.
        FileNotFoundError: If the specified file does not exist.
        AttributeError: If the point cloud object does not have the required methods.
    """
    try:
        # Opens a Phenospex PLY PointCount file
        pcd = o3d.io.read_point_cloud(sys.argv[1])
    except IndexError:
        raise IndexError("Please provide a path to the PLY file as a command line argument.")
    except FileNotFoundError:
        raise FileNotFoundError(f"The file {sys.argv[1]} does not exist.")

    # Visualize the original point cloud
    o3d.visualization.draw_geometries([pcd])

    # Store current colors
    colors = pcd.colors

    try:
        # Calculate colors+nir based on wavelength data
        pcd.wavelengthsToData()
    except AttributeError:
        raise AttributeError("The point cloud object does not have the method 'wavelengthsToData'.")
    
    # Visualize the point cloud with updated data
    o3d.visualization.draw_geometries([pcd])

    try:
        # Compute NDVI based on wavelength data (R + nir)
        pcd.computeNDVI()
    except AttributeError:
        raise AttributeError("The point cloud object does not have the method 'computeNDVI'.")
    
    # Update colors to NDVI and visualize
    pcd.colors = pcd.ndvi
    o3d.visualization.draw_geometries([pcd])

    try:
        # Scale single-channel NDVI into RGB
        pcd.colorizeNDVI()
    except AttributeError:
        raise AttributeError("The point cloud object does not have the method 'colorizeNDVI'.")
    
    # Update colors to colorized NDVI and visualize
    pcd.colors = pcd.ndvi
    o3d.visualization.draw_geometries([pcd])

if __name__ == "__main__":
    main()